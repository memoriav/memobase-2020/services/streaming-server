# Streaming Server

Memoriav Media Streaming Server

## Installation
    go get gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server
    go build gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server/cmd/server

## Test
    go test gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server/cmd/server
    
## Prerequsites

MariaDB/MySQL Server with table structure of [table.sql](tables.sql) 
  
## Server Commands

### clearcache    
    https://media.memobase.k8s.unibas.ch/command/clearcache?token=XXX

Token Subject: ```cmd:clearcache```
        
Clears the internal media cache of the server. 
should be done after changeing database contents.

### testdata    
    https://media.memobase.k8s.unibas.ch/command/testdata?type=[image|video]&access=[public|closed]&lifetime=<seconds>&num=<anzahl>&token=XXX

Token Subject: ```cmd:testdata```
        
delivers json file with testdata (including available metadata)

    
### media query

    https://media.memobase.k8s.unibas.ch/memo/<signature>/<command>
  
Token Subject: ```<signature>``` 

With fallback additional token claim: ```fb = <fallback-signature>```
  
* master
   
   Delivers the mediaserver's master object without any transformation.
   Mainly used for audio and video objects.
    
* viewer

   Displays viewer which is iFrame embeddable  
   Signature of Poster Image for Video/Audio-Player: `<signature>-poster`
      
    
* resize
  
  Default command for image retrieval. Needs obligatory size parameter.
  
        http://localhost:88/memo/sig-5005/resize/size800x200/stretch/formatJPEG      
        http://localhost:88/memo/sig-5005/resize/size800x200/stretch/formatJPEG/fallbacksig-5006      
* iiif

  sends parameters directly to iiif image server
  
        http://localhost:88/memo/sig-5005/iiif/full/800,200/0/default.jpg
        http://localhost:88/memo/sig-5005/iiif/info.json
           
* manifest
  return predefined iiif manifest if available
  
        http://localhost:88/memo/sig-5005/manifest/v2
        http://localhost:88/memo/sig-5005/manifest/v3
               
## Syntax

### Notational Conventions and Generic Grammar
Grammar is based on the Grammar used in RFC1616
https://tools.ietf.org/html/rfc2616#page-14

~~~~   
   memo_Url = serverBase "/" (dataReqest | serverCommand) 
   serverCommand = command-Base "/" (clearcache) token
   command-Base = TEXT
   clearcache = "clearcache"
   dataRequest = data-Base "/" signature "/" media-Command [token]
   data-Base = TEXT
   signature = TEXT
   media-Command = master | resize | iiif | viewer | manifest
   viewer = "viewer" ["/" fallback] 
   master = "master" ["/" fallback]
   resize = "resize" "/" size ["/" (stretch | crop)] ["/" flip] ["/" format] ["/" fallback]
   manifest = "manifest" "/" ("v2" | "v3") ["/" fallback]
   size = "size" width "x" height
   width = 1*DIGIT
   height = 1*DIGIT
   stretch = "stretch"
   crop = "crop"
   flip = "flip"
   format = "format" ("PNG" | "JPEG")
   fallback = "fallback" signature
   iiif = "iiif" "/" (iiif-ImageRequest | iiif-InformationRequest)
   iiif-InformationRequest = "info.json"
   iiif-ImageRequest = iiifregion "/" iiifsize "/" iiifrotation "/" iiifquality "." iiifformat
      ; please refer to iiif api - https://iiif.io/api/image/2.0/#uri-syntax
   token = "?token=" HEX
~~~~    

    
### Docker

```sh
docker run -v<path_to_config>:/app/memostream.toml <image_name>
```