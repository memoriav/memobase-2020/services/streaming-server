$(document).ready(function () {
  let viewer;

  $('.openseadragon-viewer').each(function () {
    let openseadragon = $(this);

    $.ajax({
      type: 'GET',
      url: 'https://media-test.memobase.k8s.unibas.ch/memo/' + openseadragon.attr('data-image-id') + '/iiif/info.json?auth=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJiYXotYl9tZWktbWVpXzQ5OTA1YS0xIn0.NT1Q2AZi5hxComXuAZ0thVp3RbpKfhvgCi6fFz0zxo0',
      success: function (data) {
        let options = {
          id: openseadragon.attr('data-image-id'),
          tileSources: data,
          fullPageButton: "full-page",
          homeButton: "reset",
          zoomInButton: "zoom-in",
          zoomOutButton: "zoom-out",
          toolbar: 'openseadragon-menu',
        };
        viewer = new OpenSeadragon.Viewer(options);
        viewer.addHandler('full-page', function() {
          if(!viewer.isFullPage()) {
            window.parent.postMessage('removeFullScreen', '*');
          }
        });

      }
    });
  });

  $('#iiif-full-page').on('click',function () {
    viewer.setFullScreen(true);
    window.parent.postMessage('goFullScreen', '*');
  });

});

