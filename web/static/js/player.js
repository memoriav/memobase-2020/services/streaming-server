$(document).ready(function () {
  //Audio, Videos (Youtube, Vimeo, Local)
  $('.player').each(function () {
    let controls = [
      'play-large', // The large play button in the center
      'play', // Play/pause playback
      'progress', // The progress bar and scrubber for playback and buffering
      'current-time', // The current time of playback
      'duration', // The full duration of the media
      'mute', // Toggle mute
      'volume', // Volume control
      'fullscreen', // Toggle fullscreen
    ];

    if(location.hash) {
      // Change the source for local audios and local videos
      if($(this).find('source').length > 0) {
        let current_src = $(this).find('source')[0].src;
        // Add to current source #t=start,end
        $(this).find('source')[0].src = current_src + location.hash;
        // Reload the media element
        $(this)[0].load();
      }
    }
    const player = new Plyr($(this), {controls});
    player.on('loadeddata', function () {
      let url_string = window.location.href;
      let url = new URL(url_string);
      let time = parseInt(url.searchParams.get("time"));

      if (time <= player.duration) {
        player.currentTime = time;
      }
    });
  });
});
