// This file is part of Memobase Mediaserver which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memostream

import (
	"net"
	"time"
)
/*
connection enhancment to make sure, that unused connections
will be closed afert timeout
 */
type IdleTimeoutConn struct {
	conn        net.Conn
	idleTimeout time.Duration
}

func NewIdleTimeoutConn( conn net.Conn, idleTimeout time.Duration) IdleTimeoutConn {
	return IdleTimeoutConn{
		conn:        conn,
		idleTimeout: idleTimeout,
	}
}

func (itc IdleTimeoutConn) Read(buf []byte) (int, error) {
	itc.conn.SetDeadline(time.Now().Add(itc.idleTimeout))
	return itc.conn.Read(buf)
}

func (itc IdleTimeoutConn) Write(buf []byte) (int, error) {
	itc.conn.SetDeadline(time.Now().Add(itc.idleTimeout))
	return itc.conn.Write(buf)
}

