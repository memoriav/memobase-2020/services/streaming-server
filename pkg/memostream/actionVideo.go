package memostream

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

// image Action using IIIF server for image manipulation
type ActionVideo struct {
	ms *memoServer
}

func NewActionVideo(ms *memoServer) *ActionVideo {
	av := &ActionVideo{ms: ms}
	return av
}

func (av *ActionVideo) GetType() []string { return []string{"video"} }

func (av *ActionVideo) Do(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	switch action {
	case "viewer":
		return av.viewer(w, req, me, action, params...)
	default:
	}
	return false, nil
}

func (av *ActionVideo) viewer(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	type vData struct {
		BackgroundColor string
		VideoSource     string
		PosterSource    string
		VideoType       string
		BaseUrl         string
		StaticPrefix    string
		End             int64
		Start           int64
	}

	// todo: Bad Hack, should be http
	scheme := "https"
	if req.TLS != nil {
		scheme = "https"
	}

	var start, end int64
	values := me.URI.Query()
	for key, vals := range values { // req.URL.Query() {
		for _, val := range vals {
			switch key {
			case "start":
				start, _ = strconv.ParseInt(val, 10, 64)
			case "end":
				end, _ = strconv.ParseInt(val, 10, 64)
			case "auth":
			default:
				// add only key if not already set
				if values.Get(key) == "" {
					values.Add(key, val)
				}
			}
		}
	}

	var err error
	newtoken := ""
	if me.Access != Media_Public {
		newtoken, err = NewJWT(av.ms.jwtSecret, me.Signature, av.ms.jwtAlg[0], 7200, "", "")
		if err != nil {
			return false, fmt.Errorf("Cannot create token: %v", err)
		}
	}
	posterurl := ""
	poster, err := av.ms.resolver.resolve(me.Signature + "-poster")
	if err == nil {
		posterurl = fmt.Sprintf("%s://%s/%s/%s/resize/size%vx%v/formatJPEG",
			scheme,
			req.Host,
			strings.Trim(av.ms.urlPrefix, "/"),
			poster.Signature,
			me.Width,
			me.Height)
		if poster.Access != Media_Public {
			postertoken, err := NewJWT(av.ms.jwtSecret, poster.Signature, av.ms.jwtAlg[0], 7200, "", "")
			if err != nil {
				return false, fmt.Errorf("Cannot create token: %v", err)
			}
			posterurl += fmt.Sprintf("?auth=%v", postertoken)
		}
	}

	data := vData{
		BackgroundColor: "#000000",
		VideoSource: fmt.Sprintf("%s://%s/%s/%s/master?auth=%s",
			scheme,
			req.Host,
			strings.Trim(av.ms.urlPrefix, "/"),
			me.Signature,
			newtoken,
			/* values.Encode(), */
		),
		PosterSource: posterurl,
		VideoType:    me.Mimetype,
		BaseUrl:      av.ms.baseUrl,
		StaticPrefix: av.ms.staticPrefix,
		Start:        start,
		End:          end,
	}
	av.ms.viewerTemplates["video"].Execute(w, data)
	return true, nil
}
