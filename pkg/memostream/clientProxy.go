package memostream

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

func clientProxy(url url.URL, w http.ResponseWriter, req *http.Request) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// you can reassign the body if you need to parse it as multipart
	req.Body = ioutil.NopCloser(bytes.NewReader(body))

	proxyReq, err := http.NewRequest(req.Method, url.String(), bytes.NewReader(body))
	if err != nil {
		http.Error(w, fmt.Sprintf("cannot create request for %s: %s", url.String(), err.Error()), http.StatusBadGateway)
		return
	}

	// filter here if necessary
	proxyReq.Header = make(http.Header)
	for h, val := range req.Header {
		proxyReq.Header[h] = val
	}
	httpClient := http.Client{
		Transport: http.DefaultTransport,
		Timeout:   time.Hour,
	}
	resp, err := httpClient.Do(proxyReq)
	if err != nil {
		http.Error(w, fmt.Sprintf("cannot request %s: %s", url.String(), err.Error()), http.StatusBadGateway)
		return
	}
	defer resp.Body.Close()

	_, err = io.Copy(w, resp.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("cannot copy data for request %s: %s", url.String(), err.Error()), http.StatusBadGateway)
		return
	}
}
