package memostream

import (
	"fmt"
	"net/http"
)

// image Action using IIIF server for image manipulation
type ActionMaster struct {
	ms *memoServer
}

func NewActionMaster(ms *memoServer) *ActionMaster {
	am := &ActionMaster{ms: ms}
	return am
}

func (am *ActionMaster) GetType() []string { return []string{"image", "video", "audio", "unknown"} }

func (am *ActionMaster) Do(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	if action != "master" && action != "" {
		return false, nil
	}

	values := me.URI.Query()
	for key, vals := range req.URL.Query() {
		for _, val := range vals {
			if key == "auth" {
				continue
			}
			if values.Get(key) == "" {
				values.Add(key, val)
			}
		}
	}
	u := me.URI
	u.RawQuery = values.Encode()
	switch me.Protocol {
	case Media_File: // stream the file directly
		w.Header().Set("Content-Disposition", fmt.Sprintf("filename=\"%s\"", me.Signature))
		reader, modTime, err := am.ms.fspool.OpenUri(me.URI)
		if err != nil {
			return false, fmt.Errorf("cannot open uri %s: %v", me.URI, err)
		}
		defer reader.Close()
		http.ServeContent(w, req, me.Signature, modTime, reader)
	case Media_Redirect: // just redirect
		http.Redirect(w, req, u.String(),
			http.StatusTemporaryRedirect)
	case Media_Proxy: // do all the proxy stuff
		//		w.Header().Set("Content-Disposition", fmt.Sprintf("filename=\"%s\"", me.Signature))
		requestProxy(&u, w, req, am.ms.insecureProxy, am.ms.log)
	case Media_ProxyDirect: // do all the proxy stuff
		//		w.Header().Set("Content-Disposition", fmt.Sprintf("filename=\"%s\"", me.Signature))
		reverseProxy(&u, w, req, am.ms.insecureProxy, am.ms.log)
	default: // don't know what to do
		return false, fmt.Errorf("only type \"file\", \"redirect\" and \"proxy\" is supported for signature %v: type %v given", me.Signature, me.getProtocolString())
	}

	return true, nil
}
