package memostream

import (
	"fmt"
	"net/http"
	"regexp"
)

// image Action using IIIF server for image manipulation
type ActionExternal struct {
	ms *memoServer
}

func NewActionExternal(ms *memoServer) *ActionExternal {
	ae := &ActionExternal{ms: ms}
	return ae
}

func (ae *ActionExternal) GetType() []string {
	return []string{"srfaudio", "srfvideo", "youtube", "vimeo", "zhdk"}
}

func (ae *ActionExternal) Do(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	switch action {
	case "viewer":
		return ae.viewer(w, req, me, action, params...)
	default:
	}
	return false, nil
}

var extMatches = map[string]*regexp.Regexp{
	"srfvideo": regexp.MustCompile("^.+(urn:(rti|rtr|rts|srf):video:[-0-9a-f]+)"),
	"srfaudio": regexp.MustCompile("^.+(urn:(rti|rtr|rts|srf):audio:[-0-9a-f]+)"),
	"youtube":  regexp.MustCompile("^http.+/([^/]+)$"),
	"vimeo":    regexp.MustCompile("^http.+/([^/]+)$"), // https://vimeo.com/289691364
	"zhdk":     regexp.MustCompile("^(.+)$"),
}

func (ae *ActionExternal) viewer(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	type vData struct {
		Id              string
		BackgroundColor string
		BaseUrl         string
		StaticPrefix    string
	}

	data := vData{
		BackgroundColor: "#000000",
		BaseUrl:         ae.ms.baseUrl,
		StaticPrefix:    ae.ms.staticPrefix,
	}

	rexp, ok := extMatches[me.Type]
	if !ok {
		return false, fmt.Errorf("no id regexp for type %s - %v", me.Type, me.Signature)
	}
	matches := rexp.FindStringSubmatch(me.URI.String())
	if matches != nil {
		data.Id = matches[1]
	}
	tpl, ok := ae.ms.viewerTemplates[me.Type]
	if !ok {
		return false, fmt.Errorf("no template for type %s - %v", me.Type, me.Signature)
	}
	tpl.Execute(w, data)
	return true, nil
}
