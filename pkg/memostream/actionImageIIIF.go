package memostream

import (
	"errors"
	"fmt"
	"github.com/goph/emperror"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
)

// image Action using IIIF server for image manipulation
type ActionImageIIIF struct {
	ms *memoServer
}

func NewActionImageIIIF(ms *memoServer) *ActionImageIIIF {
	iai := &ActionImageIIIF{ms: ms}
	return iai
}

func (iai *ActionImageIIIF) GetType() []string { return []string{"image"} }

var regexpParamImageActionIIIF = []*regexp.Regexp{
	regexp.MustCompile("^(size)([0-9]+)x([0-9]+)$"),
	regexp.MustCompile("^(rotate)([0-9.]+)$"),
	regexp.MustCompile("^(format)(jpeg|png)$"),
	regexp.MustCompile("^(color)(|gray|bitonal)?$"),
	regexp.MustCompile("^(stretch)$"),
	regexp.MustCompile("^(crop)$"),
	regexp.MustCompile("^(flip)$"),
}

func (iai *ActionImageIIIF) Do(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	switch action {
	case "resize":
		return iai.resize(w, req, me, action, params...)
	case "viewer":
		return iai.viewer(w, req, me, action, params...)
	case "iiif":
		return iai.iiif(w, req, me, action, params...)
	default:
	}
	return false, nil
}
func (iai *ActionImageIIIF) iiif(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	var file string
	if me.Protocol == Media_File {
		if !iai.ms.fspool.Exist(me.URI) {
			return false, fmt.Errorf("file not found: %v", me.URI)
		}
		file = filepath.ToSlash(iai.ms.fspool.Truename(me.URI))
	} else {
		file = me.URI.String()
	}

	// check for strange params like ".."
	for _, p := range params {
		if strings.Trim(p, ". ") == "" {
			return false, fmt.Errorf("invalid parameter %s", p)
		}
	}
	if _, err := iai.ms.proxyIIIF(req, w, me.Signature, file, strings.Join(params, "/"), me.Access == Media_Public); err != nil {
		return false, fmt.Errorf("cannot proxy to iiif server: %v", err)
	}
	return true, nil

}

func (iai *ActionImageIIIF) viewer(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	type vData struct {
		BackgroundColor string
		Id              string
		TileSource      string
		BaseUrl         string
		StaticPrefix    string
	}
	var err error
	newtoken := ""
	if me.Access != Media_Public {
		newtoken, err = NewJWT(iai.ms.jwtSecret, me.Signature, iai.ms.jwtAlg[0], 7200, "", "")
		if err != nil {
			return false, fmt.Errorf("Cannot create token: %v", err)
		}
	}

	tileSource := fmt.Sprintf("%s/%s/%s/iiif/info.json?auth=%s",
		strings.TrimRight(iai.ms.baseUrl, "/"),
		strings.Trim(iai.ms.urlPrefix, "/"),
		me.Signature,
		newtoken)
	if me.Protocol == Media_Redirect {
		tileSource = me.URI.String()
	}
	data := vData{
		BackgroundColor: "#000000",
		Id:              me.Signature,
		TileSource:      tileSource,
		BaseUrl:         iai.ms.baseUrl,
		StaticPrefix:    iai.ms.staticPrefix,
	}
	iai.ms.viewerTemplates["image"].Execute(w, data)
	return true, nil
}

func (iai *ActionImageIIIF) resize(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	ps := map[string][]string{}
	// create array of parameters
	for _, p := range params {
		for _, rexp := range regexpParamImageActionIIIF {
			res := rexp.FindStringSubmatch(strings.ToLower(p))
			if res != nil {
				ps[res[1]] = res[2:]
				break
			}
		}
	}

	// A thumbnail is an otherwise unaltered representation of an image with size 640x480
	wantsThumbnail := true

	// calculate width and height from resize parameter
	var width, height int64
	var err error
	resizeParams, ok := ps["size"]
	if !ok {
		// without resize we cannot do anything
		return false, nil
	}
	width, err = strconv.ParseInt(resizeParams[0], 10, 64)
	if err != nil {
		return false, emperror.Wrapf(err, "invalid width %v in resize", resizeParams[0])
	}
	height, err = strconv.ParseInt(resizeParams[1], 10, 64)
	if err != nil {
		return false, emperror.Wrapf(err, "invalid height %v in resize", resizeParams[1])
	}

	if width != 640 || height != 480 {
		wantsThumbnail = false
	}

	size := fmt.Sprintf("%v,%v", width, height)
	var format string
	if formatParams, ok := ps["format"]; ok {
		format = strings.ToLower(formatParams[0])
		wantsThumbnail = false
	}

	var ext string
	switch format {
	case "png":
		ext = "png"
		wantsThumbnail = false
	default:
		ext = "jpg"
	}

	// if not stretch, we need !size
	if _, ok := ps["stretch"]; !ok {
		size = "!" + size
	} else {
		wantsThumbnail = false
	}

	rotate := "0"
	if rot, ok := ps["rotate"]; ok {
		rotate = rot[0]
		wantsThumbnail = false
	}

	if _, ok := ps["flip"]; ok {
		rotate = "!" + rotate
		wantsThumbnail = false
	}

	color := "default"
	if col, ok := ps["color"]; ok {
		if len(col) > 0 {
			color = col[0]
		}
		if color == "" {
			color = "color"
		}
		wantsThumbnail = false
	}

	resultName := fmt.Sprintf("%s_%vx%v.%s", me.Signature, width, height, ext)
	w.Header().Set("Content-Disposition", fmt.Sprintf("filename=\"%s\"", resultName))

	// build param string
	var paramstring string

	region := "full"
	if _, ok := ps["crop"]; ok {
		// for cropping in the middle, we need region parameters
		aspectOrig := float64(me.Width) / float64(me.Height)
		aspectNew := float64(width) / float64(height)
		var nX, nY, nW, nH int64
		if aspectOrig > aspectNew {
			// target is smaller than source - vertical fill
			nY = 0
			nH = me.Height
			nW = int64(float64(nH) * aspectNew)
			nX = (me.Width - nW) / 2
		} else {
			// source is smaller than target - horizontal fill
			nX = 0
			nW = me.Width
			nH = int64(float64(nW) / aspectNew)
			nY = (me.Height - nH) / 2
		}
		region = fmt.Sprintf("%v,%v,%v,%v", nX, nY, nW, nH)
		wantsThumbnail = false
	}

	// todo: region & rotation parameter
	paramstring = fmt.Sprintf("%s/%v/%v/%s.%s", region, size, rotate, color, ext)

	var sigfile string
	if me.Protocol == Media_File {
		if !iai.ms.fspool.Exist(me.URI) {
			return false, fmt.Errorf("file not found: %v", me.URI)
		}
		sigfile = iai.ms.fspool.Truename(me.URI) // me.getFilePath()
	} else {
		sigfile = me.URI.String()
	}

	//sigfile := iai.ms.fspool.Truename(me.URI)
	sigfile = strings.TrimPrefix(sigfile, iai.ms.iiifBase)
	sigfile = strings.Replace(sigfile, "/", "%24", -1)

	if wantsThumbnail {
		if ok, err := iai.tryFetchCachedThumbnail(w, me); ok && err == nil {
			return true, nil
		} else if err != nil {
			iai.ms.log.Error("Cannot fetch cached thumbnail")
			return false, fmt.Errorf("cannot fetch cached thumbnail: %v", err)
		}
	}
	iai.ms.log.Info("Fetching file from image server")
	if buf, err := iai.ms.proxyIIIF(req, w, me.Signature, sigfile, paramstring, me.Access == Media_Public); err != nil {
		return false, fmt.Errorf("cannot proxy request: %v", err)
	} else if wantsThumbnail {
		iai.cacheThumbnail(buf, me)
	}

	return true, nil
}

func (iai *ActionImageIIIF) tryFetchCachedThumbnail(w http.ResponseWriter, me *MediaEntry) (bool, error) {
	cachedUri := iai.generatePathToCachedImage(me)
	if _, err := os.Stat(cachedUri); errors.Is(err, os.ErrNotExist) {
		iai.ms.log.Debugf("Thumbnail %v is not yet cached", cachedUri)
		return false, nil
	}
	fileBytes, err := ioutil.ReadFile(cachedUri)
	if err != nil {
		iai.ms.log.Errorf("File read error %v", cachedUri)
		return false, err
	}
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", me.Mimetype)
	w.Write(fileBytes)
	iai.ms.log.Debugf("Thumbnail for %v already in cache", cachedUri)
	return true, nil
}

func (iai *ActionImageIIIF) cacheThumbnail(imageBuf []byte, me *MediaEntry) (bool, error) {
	cachedUri := iai.generatePathToCachedImage(me)
	if l := len(imageBuf); l < (8 * 1024) {
		iai.ms.log.Infof("Size of %v (%v) is too small to qualify to be cached", cachedUri, l)
		return false, nil
	} else if mimetype := http.DetectContentType(imageBuf); mimetype != "image/jpeg" && mimetype != "image/png" {
		iai.ms.log.Errorf("File %v doesn't seem to be an image file: %v", cachedUri, mimetype)
	}
	f, err := os.Create(cachedUri)
	if err != nil {
		iai.ms.log.Errorf("An error occurred when creating cache file: %v", err)
		return false, err
	}
	if _, err := f.Write(imageBuf); err != nil {
		iai.ms.log.Errorf("An error occurred when writing cache file: %v", err)
		return false, err
	}
	if err := f.Close(); err != nil {
		iai.ms.log.Errorf("An error occurred when closing cache file: %v", err)
		return false, err
	}
	return true, nil
}

func (iai *ActionImageIIIF) generatePathToCachedImage(me *MediaEntry) string {
	if me.URI.Scheme == "http" || me.URI.Scheme == "https" {
		var mimetype string
		if me.Mimetype == "image/jpeg" || me.Mimetype == "image/jpg" {
			mimetype = "jpg"
		} else {
			mimetype = "png"
		}
		return fmt.Sprintf("%v%v.%v", iai.ms.iiifCached, me.Signature, mimetype)
	} else if me.URI.Scheme == "file" {
		return strings.Replace(me.URI.String()[7:], iai.ms.iiifBase, iai.ms.iiifCached, 1)
	}
	return me.URI.String()
}
