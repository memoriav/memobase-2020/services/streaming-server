package memostream

import "net/http"

// interface for a generic Action on a media object
type Action interface {
	// media types which are allowed for this Action
	GetType() []string
	// execute the Action
	Do(w http.ResponseWriter, req *http.Request, m *MediaEntry, action string, params ...string) (bool, error)
}
