package memostream

import (
	"github.com/op/go-logging"
	"net/http"
	"net/http/httputil"
	"net/url"
)

// proxy to given url
func reverseProxy(url *url.URL, w http.ResponseWriter, r *http.Request, insecure bool, log *logging.Logger) {
	log.Debugf("reverseProxy %s --> %s - %v", r.URL.String(), url.String(), r.Header)
	// build a proxy director, which connects to our target url
	director := func(req *http.Request) {
		req.URL.Scheme = url.Scheme
		req.URL.Host = url.Host
		req.URL.Path = url.Path
		req.URL.RawQuery = url.RawQuery
		//		req.Header.Set("X-Forwarded-For", "")
		//		req.Header["X-Forwarded-For"] = nil
		if _, ok := req.Header["User-Agent"]; !ok {
			// explicitly disable User-Agent so it's not set to default value
			req.Header.Set("User-Agent", "")
		}
	}

	modifyResponse := func(resp *http.Response) error {
		return nil
	}

	// create the reverse proxy
	rp := httputil.ReverseProxy{Director: director, ModifyResponse: modifyResponse}

	tr, ok := http.DefaultTransport.(*http.Transport)
	if !ok {
		log.Error("http.DefaultTransport is not (*http.Transport)")
		http.Error(w, "http.DefaultTransport is not (*http.Transport)", 500)
		return
	}
	tr = tr.Clone()
	tr.TLSClientConfig.InsecureSkipVerify = insecure

	//rp.Transport = &transport{http.DefaultTransport}
	rp.Transport = &transport{tr}

	// and go for it
	// should we use IdleTimeoutConn? Paranoia?
	rp.ServeHTTP(w, r)
}
