// This file is part of Memobase Mediaserver which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memostream

import (
	"encoding/json"
	"expvar"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
	"strings"
)

func (ms *memoServer) TestData(w http.ResponseWriter, req *http.Request) {
	query := req.URL.Query()
	var mediatype string
	types, ok := query["type"]
	if ok && len(types) > 0 {
		mediatype = types[0]
	}

	var access string
	accesss, ok := query["access"]
	if ok && len(accesss) > 0 {
		access = accesss[0]
	}

	var num int64 = 500
	nums, ok := query["num"]
	if ok && len(nums) > 0 {
		var err error
		num, err = strconv.ParseInt(nums[0], 10, 64)
		if err != nil {
			ms.DoPanicf(w, http.StatusBadRequest, "invalid parameter for num=%v", nums[0])
			return
		}
	}

	var lifetime int64
	lifetimes, ok := query["lifetime"]
	if ok && len(lifetimes) > 0 {
		var err error
		lifetime, err = strconv.ParseInt(lifetimes[0], 10, 64)
		if err != nil {
			ms.DoPanicf(w, http.StatusBadRequest, "invalid parameter for lifetime=%v", lifetimes[0])
			return
		}
	}
	if lifetime == 0 {
		lifetime = 3600
	}

	if num > 500 {
		num = 500
	}

	entries, err := ms.resolver.TestData(mediatype, access, num)
	if err != nil {
		ms.DoPanicf(w, http.StatusInternalServerError, "cannot load testdata: %v", err)
		return
	}
	scheme := "http"
	if req.TLS != nil {
		scheme = "https"
	}
	type resultEntry struct {
		Url                     string
		Width, Height, Duration int64
		Access                  string
		Type                    string
		Mimetype                string
	}
	result := []resultEntry{}
	for _, me := range entries {
		url := fmt.Sprintf("%s://%s/%s/%s/master",
			scheme,
			req.Host,
			strings.Trim(ms.urlPrefix, "/"),
			me.Signature)
		if me.Access == Media_Private {
			newtoken, err := NewJWT(ms.jwtSecret, me.Signature, ms.jwtAlg[0], lifetime, "", "")
			if err != nil {
				ms.DoPanicf(w, http.StatusInternalServerError, "cannot create jwt: %v", err)
			}
			url += fmt.Sprintf("?auth=%v", newtoken)
		}
		result = append(result, resultEntry{
			Url:      url,
			Width:    me.Width,
			Height:   me.Height,
			Duration: me.Duration,
			Type:     me.Type,
			Access:   MediaAccessNum[me.Access],
			Mimetype: me.Mimetype,
		})

	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	jsonEnc := json.NewEncoder(w)
	jsonEnc.SetIndent("", "  ")
	jsonEnc.Encode(result)
}

func (ms *memoServer) commandHandler(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	cmd, ok := vars["cmd"]
	if !ok {
		errStr := fmt.Sprintf("no command given")
		ms.log.Error(errStr)
		http.Error(w, errStr, http.StatusNotFound)
		return
	}
	if err := CheckRequestJWT(req, ms.jwtSecret, ms.jwtAlg, "cmd:"+cmd, "sub"); err != nil {
		errStr := fmt.Sprintf("Access denied: token check failed: %v", err)
		ms.log.Error(errStr)
		http.Error(w, errStr, http.StatusForbidden)
		return
	}

	switch cmd {
	case "testdata":
		ms.TestData(w, req)
	case "clearcache":
		ms.ClearCache()
		ms.log.Info("cache cleared")
		w.Write([]byte("ok"))
	case "metrics":
		w.Header().Set("Content-Type", "application/json; charset=utf-8")

		first := true
		report := func(key string, value interface{}) {
			if !first {
				fmt.Fprintf(w, ",\n")
			}
			first = false
			if str, ok := value.(string); ok {
				fmt.Fprintf(w, "%q: %q", key, str)
			} else {
				fmt.Fprintf(w, "%q: %v", key, value)
			}
		}

		fmt.Fprintf(w, "{\n")
		expvar.Do(func(kv expvar.KeyValue) {
			report(kv.Key, kv.Value)
		})
		fmt.Fprintf(w, "\n}\n")
	default:
		errStr := fmt.Sprintf("unknown command: %ms", cmd)
		ms.log.Error(errStr)
		http.Error(w, errStr, http.StatusNotImplemented)
		return
	}
}
