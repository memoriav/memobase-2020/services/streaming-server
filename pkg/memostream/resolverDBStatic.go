// This file is part of Memobase Mediaserver which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memostream

import (
	"errors"
	"fmt"
	"github.com/goph/emperror"
	"net/url"
)

/**
Static resolver for testing the memoServer functionality
*/
type ResolverDBStatic struct {
	signatures map[string]Sig
}

func (rds *ResolverDBStatic) TestData(mediatype, access string, num int64) ([]*MediaEntry, error) {
	panic("implement me")
}

func NewResolverDBStatic(signatures map[string]Sig) *ResolverDBStatic {
	return &ResolverDBStatic{signatures: signatures}
}
func (rds *ResolverDBStatic) Close() {
	//  nothing to do
}

func (rds *ResolverDBStatic) Resolve(signature string) (*MediaEntry, error) {
	sig, ok := rds.signatures[signature]
	if !ok {
		return nil, errors.New(fmt.Sprintf("unknown signature %s", signature))
	}
	u, err := url.Parse(sig.Uri)
	if err != nil {
		return nil, emperror.Wrapf(err, "cannot parse url %s of signature %s", sig.Uri, signature)
	}
	if u == nil {
		return nil, emperror.Wrapf(err, "cannot parse url %s of signature %s", sig.Uri, signature)
	}
	p, ok := MediaProtocolString[sig.Type]
	if !ok {
		return nil, errors.New(fmt.Sprintf("invalid protocol %s for signature %s", sig.Type, signature))
	}
	a, ok := MediaAccessString[sig.Access]
	if !ok {
		return nil, errors.New(fmt.Sprintf("invalid access %s for signature %s", sig.Access, signature))
	}
	return &MediaEntry{
		Signature: signature,
		URI:       *u,
		Protocol:  p,
		Access:    a,
	}, nil
}
