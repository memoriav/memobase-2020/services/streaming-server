package memostream

import (
	"fmt"
	"io"
	"net/url"
	"time"
)

// io.ReadCloser with io.seeker for http.ServeContent
type ReadSeekerCloser interface {
	io.ReadCloser
	io.Seeker
}

// abstraction interface for creating ReadSeekerCloser
type Filesystem interface {
	// creates ReadSeekerCloser, returns last modification time
	Open(uri url.URL) (ReadSeekerCloser, time.Time, error)

	// checks existence of uri object
	Exist(uri url.URL) bool

	// builds native access path
	Truename(uri url.URL) string
}

// pool of filesystems
type FilesystemPool struct {
	// fspool from uri-scheme to Filesystem
	fss map[string]Filesystem
}

// constructor of FilesystemPool
func NewFilesystemPool() *FilesystemPool {
	return &FilesystemPool{fss: map[string]Filesystem{}}
}

// set a Filesystem for a specified uri scheme
func (fp *FilesystemPool) SetFilesystem(scheme string, fs Filesystem) {
	fp.fss[scheme] = fs
}

func (fp *FilesystemPool) Truename(uri url.URL) string {
	fs, ok := fp.fss[uri.Scheme]
	if !ok {
		return "invalid"
	}
	return fs.Truename(uri)
}

func (fp *FilesystemPool) Exist(uri url.URL) bool {
	fs, ok := fp.fss[uri.Scheme]
	if !ok {
		return false
	}
	return fs.Exist(uri)
}

// creates a ReadSeekerCloser for a specified uri from matching Filesystem
func (fp *FilesystemPool) OpenUri(uri url.URL) (ReadSeekerCloser, time.Time, error) {
	fs, ok := fp.fss[uri.Scheme]
	if !ok {
		return nil, time.Now(), fmt.Errorf("unknown uri scheme %s from url %s", uri.Scheme, uri.String())
	}
	return fs.Open(uri)
}
