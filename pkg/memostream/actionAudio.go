package memostream

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

// image Action using IIIF server for image manipulation
type ActionAudio struct {
	ms *memoServer
}

func NewActionAudio(ms *memoServer) *ActionAudio {
	aa := &ActionAudio{ms: ms}
	return aa
}

func (aa *ActionAudio) GetType() []string { return []string{"audio"} }

func (aa *ActionAudio) Do(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	switch action {
	case "viewer":
		return aa.viewer(w, req, me, action, params...)
	default:
	}
	return false, nil
}

func (aa *ActionAudio) viewer(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	type vData struct {
		AudioSource     string
		PosterSource    string
		AudioType       string
		BackgroundColor string
		BaseUrl         string
		StaticPrefix    string
		End             int64
		Start           int64
	}

	// todo: Bad Hack, should be http
	scheme := "https"
	if req.TLS != nil {
		scheme = "https"
	}

	var start, end int64
	values := me.URI.Query()
	for key, vals := range req.URL.Query() {
		for _, val := range vals {
			switch key {
			case "start":
				start, _ = strconv.ParseInt(val, 10, 64)
			case "end":
				end, _ = strconv.ParseInt(val, 10, 64)
			case "auth":
			default:
				if values.Get(key) == "" {
					values.Add(key, val)
				}
			}
		}
	}

	var err error
	newtoken := ""
	if me.Access != Media_Public {
		newtoken, err = NewJWT(aa.ms.jwtSecret, me.Signature, aa.ms.jwtAlg[0], 7200, "", "")
		if err != nil {
			return false, fmt.Errorf("Cannot create token: %v", err)
		}
	}

	posterurl := ""
	poster, err := aa.ms.resolver.resolve(me.Signature + "-poster")
	if err == nil {
		posterurl = fmt.Sprintf("%s://%s/%s/%s/resize/size%vx%v/formatJPEG",
			scheme,
			req.Host,
			strings.Trim(aa.ms.urlPrefix, "/"),
			poster.Signature,
			poster.Width,
			poster.Height)
		if poster.Access != Media_Public {
			postertoken, err := NewJWT(aa.ms.jwtSecret, poster.Signature, aa.ms.jwtAlg[0], 7200, "", "")
			if err != nil {
				return false, fmt.Errorf("Cannot create token: %v", err)
			}
			posterurl += fmt.Sprintf("?auth=%v", postertoken)
		}
	}

	data := vData{
		AudioSource: fmt.Sprintf("%s://%s/%s/%s?auth=%s",
			scheme,
			req.Host,
			strings.Trim(aa.ms.urlPrefix, "/"),
			me.Signature,
			newtoken,
		/* values.Encode(), */
		),
		AudioType:       me.Mimetype,
		BackgroundColor: "#000000",
		BaseUrl:         aa.ms.baseUrl,
		StaticPrefix:    aa.ms.staticPrefix,
		Start:           start,
		End:             end,
	}
	aa.ms.viewerTemplates["audio"].Execute(w, data)
	return true, nil
}
