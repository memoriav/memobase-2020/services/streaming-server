// This file is part of Memobase Mediaserver which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memostream

import (
	"context"
	"fmt"
	"io"
	"net"
	"net/http"
	"regexp"
	"strings"
	"text/template"

	"github.com/goph/emperror"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/op/go-logging"
)

type Sig struct {
	Type   string
	Uri    string
	Access string
}

type memoServer struct {
	srv               *http.Server
	insecureProxy     bool
	baseUrl           string
	staticDir         string
	urlPrefix         string
	cmdPrefix         string
	staticPrefix      string
	iiifPrefix        string
	iiifBase          string
	iiifCached        string
	iiifUrl           string
	iiifJwtSubPrefix  string
	iiifManifestBase  string
	iiifRenderingBase string
	viewerTemplates   map[string]*template.Template
	resolver          *ResolverCache
	host              string
	port              string
	jwtSecret         string
	jwtAlg            []string
	log               *logging.Logger
	accesslog         io.Writer
	errorTemplate     *template.Template
	fspool            *FilesystemPool
	actions           map[string][]Action
}

func NewServer(
	baseUrl,
	urlPrefix,
	cmdPrefix,
	iiifPrefix,
	iiifBase,
	iiifCached,
	iiifUrl,
	iiifJwtSubPrefix,
	iiifViewerTemplate,
	iiifManifestBase,
	iiifRenderingBase,
	addr string,
	insecureProxy bool,
	resolver *ResolverCache,
	mapping *FilesystemPool,
	jwtSecret string,
	jwtAlg []string,
	log *logging.Logger,
	accesslog io.Writer,
	errorTemplate string,
	viewer map[string]string,
	staticPrefix,
	staticDir string) (*memoServer, error) {
	urlPrefix = "/" + strings.Trim(urlPrefix, "/") + "/"
	cmdPrefix = "/" + strings.Trim(cmdPrefix, "/") + "/"
	host, port, err := net.SplitHostPort(addr)
	if err != nil {
		//log.Panicf("cannot split address %s: %v", addr, err)
		return nil, emperror.Wrapf(err, "cannot split address %s", addr)
	}

	vs := make(map[string]*template.Template)
	for key, file := range viewer {
		vs[key], err = template.ParseFiles(file)
		if err != nil {
			return nil, emperror.Wrapf(err, "cannot parse %s template %s", key, file)
		}
	}
	errorTpl, err := template.ParseFiles(errorTemplate)
	if err != nil {
		return nil, emperror.Wrapf(err, "cannot parse error template %s", errorTemplate)
	}

	return &memoServer{
		//mh:            NewMemoHandler(baseDir, urlPrefix, resolver, jwtSecret, log, errorTemplate),
		baseUrl:           baseUrl,
		resolver:          resolver,
		fspool:            mapping,
		urlPrefix:         urlPrefix,
		cmdPrefix:         cmdPrefix,
		iiifPrefix:        iiifPrefix,
		iiifBase:          iiifBase,
		iiifCached:        iiifCached,
		iiifUrl:           iiifUrl,
		iiifJwtSubPrefix:  iiifJwtSubPrefix,
		iiifManifestBase:  iiifManifestBase,
		iiifRenderingBase: iiifRenderingBase,
		viewerTemplates:   vs,
		host:              host,
		port:              port,
		insecureProxy:     insecureProxy,
		jwtSecret:         jwtSecret,
		jwtAlg:            jwtAlg,
		log:               log,
		accesslog:         accesslog,
		errorTemplate:     errorTpl,
		staticPrefix:      staticPrefix,
		staticDir:         staticDir,
		actions:           map[string][]Action{},
	}, nil
}

func (ms *memoServer) ListActions() []string {
	actions := []string{}
	for a, _ := range ms.actions {
		actions = append(actions, a)

	}
	return actions
}

// add new action
func (ms *memoServer) AddAction(a Action) {
	for _, typ := range a.GetType() {
		if _, ok := ms.actions[typ]; !ok {
			ms.actions[typ] = []Action{}
		}
		ms.actions[typ] = append(ms.actions[typ], a)
	}
}

func (ms *memoServer) DoPanicf(writer http.ResponseWriter, status int, message string, a ...interface{}) (err error) {
	msg := fmt.Sprintf(message, a...)
	ms.DoPanic(writer, status, msg)
	return
}

func (ms *memoServer) DoPanic(writer http.ResponseWriter, status int, message string) (err error) {
	type errData struct {
		Status     int
		StatusText string
		Message    string
	}
	ms.log.Error(message)
	data := errData{
		Status:     status,
		StatusText: http.StatusText(status),
		Message:    message,
	}
	writer.WriteHeader(status)
	// if there'ms no error Template, there'ms no help...
	ms.errorTemplate.Execute(writer, data)
	return
}

func (ms *memoServer) ListenAndServe(cert, key string) error {
	router := mux.NewRouter()

	// https://mediaserver.memobase.ch/[prefix]/[signature]/[action]/[params]
	mainRegexp := regexp.MustCompile(fmt.Sprintf("^/%s/([^/]+)(/([^/]+)(/(.+))?)?$", strings.Trim(ms.urlPrefix, "/")))
	router.
		MatcherFunc(func(r *http.Request, rm *mux.RouteMatch) bool {
			matches := mainRegexp.FindSubmatch([]byte(r.URL.Path))
			if len(matches) == 0 {
				return false
			}
			rm.Vars = map[string]string{}
			rm.Vars["signature"] = string(matches[1])
			rm.Vars["action"] = string(matches[3])
			rm.Vars["params"] = string(matches[5])
			return true
		}).HandlerFunc(ms.mainHandler).Methods("GET", "HEAD")
	//	router.PathPrefix(ms.urlPrefix).HandlerFunc(ms.mainHandler)

	router.HandleFunc(ms.cmdPrefix+"{cmd}", ms.commandHandler)

	// route for IIIF
	// https://mediaserver.memobase.ch/[prefix]/[signature]/[token]/iiif/2/diplomhgk%24data%24e%24e%24ee572414d3bc2bc3d7bb1b28c22afddc.tiff/512,1024,512,512/512,/0/default.jpg
	iiifRegexp := regexp.MustCompile(fmt.Sprintf("^/%s/([^/]+)/([^/]+)/([^/]+)/([^/]+)/([^/]+)(/(.+))?$", strings.Trim(ms.iiifPrefix, "/")))
	router.
		Methods("GET").
		MatcherFunc(func(r *http.Request, rm *mux.RouteMatch) bool {
			matches := iiifRegexp.FindSubmatch([]byte(r.URL.EscapedPath()))
			if len(matches) == 0 {
				return false
			}
			rm.Vars = map[string]string{}
			rm.Vars["signature"] = string(matches[1])
			rm.Vars["token"] = string(matches[2])
			rm.Vars["api"] = string(matches[3])
			rm.Vars["apiversion"] = string(matches[4])
			rm.Vars["file"] = string(matches[5])
			rm.Vars["params"] = string(matches[7])
			return true
		}).
		HandlerFunc(ms.HandlerIIIF)

	//	router.Handle("/metrics", promhttp.Handler())

	// the static fileserver
	router.
		PathPrefix(ms.staticPrefix).
		Handler(http.StripPrefix(ms.staticPrefix, http.FileServer(http.Dir(ms.staticDir))))

	// metric beat handler
	router.HandleFunc("/debug/vars", metricsHandler)

	// do some CORS stuff
	router.Use(mux.CORSMethodMiddleware(router))

	loggedRouter := handlers.LoggingHandler(ms.accesslog, router)
	addr := net.JoinHostPort(ms.host, ms.port)
	ms.srv = &http.Server{
		Handler: loggedRouter,
		Addr:    addr,
	}

	if cert != "" && key != "" {
		ms.log.Infof("starting HTTPS memoServer at https://%v", addr)
		return ms.srv.ListenAndServeTLS(cert, key)
	} else {
		ms.log.Infof("starting HTTP memoServer at http://%v", addr)
		return ms.srv.ListenAndServe()
	}
}

// helper to determine protocol, host and port. uses some heuristics
func (ms *memoServer) getProtoHostPort(req *http.Request) (proto string, host, port string) {
	var err error
	host, port, err = net.SplitHostPort(req.Host)
	if err != nil {
		host = req.Host
		port = ""
	}
	proto = "http"
	if req.TLS != nil {
		proto = "https"
	}
	return
}

// clear cache of the signature resolver
func (ms *memoServer) ClearCache() {
	ms.resolver.ClearCache()
}

func (ms *memoServer) Shutdown(ctx context.Context) error {
	return ms.srv.Shutdown(ctx)
}
