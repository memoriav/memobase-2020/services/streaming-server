// This file is part of Memobase Mediaserver which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memostream

import (
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

type transport struct {
	http.RoundTripper
}

func (t *transport) RoundTrip(req *http.Request) (resp *http.Response, err error) {
	resp, err = t.RoundTripper.RoundTrip(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

var regexpFallback = regexp.MustCompile("^fallback(.+)$")
var regexpFonoteca = regexp.MustCompile("fonoteca.ch/imgs-mpn/(.+)$")

func (ms *memoServer) mainHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	// remove prefix and use whole rest of url as signature
	vars := mux.Vars(req)
	signature, ok := vars["signature"]
	if !ok {
		ms.DoPanicf(w, http.StatusBadRequest, "no signature in url: %s", req.URL.Path)
		return
	}
	action, _ := vars["action"]
	paramstr, _ := vars["params"]
	params := strings.Split(paramstr, "/")

	subClaim := "sub"
	// resolve signature
	me, err := ms.resolver.resolve(signature)
	if err != nil {
		// check for fallback parameter only if needed
		fallback := ""
		for _, p := range params {
			if matches := regexpFallback.FindStringSubmatch(p); matches != nil {
				fallback = matches[1]
				break
			}
		}
		if fallback != "" {
			// todo: check jwt for fallback
			me, err = ms.resolver.resolve(fallback)
			if err != nil {
				ms.DoPanicf(w, http.StatusNotFound, "signature %v and fallback %v not found: %v", signature, fallback, err)
				return
			}
			subClaim = "fb"
		} else {
			ms.DoPanicf(w, http.StatusNotFound, "signature %v not found: %v", signature, err)
			return
		}
	}
	if me.Access != Media_Public {
		if err := CheckRequestJWT(req, ms.jwtSecret, ms.jwtAlg, signature, subClaim); err != nil {
			ms.DoPanicf(w, http.StatusForbidden, "Access denied: token check failed: %v", err)
			return
		}
	}

	// Fonoteca uses a caching mechanism for which a handle has to be activated before the media file
	// is publicly available.
	fonotecaMatches := regexpFonoteca.FindStringSubmatch(me.URI.String())
	if fonotecaMatches != nil {
		ms.log.Info("Fonoteca object found. Activating cache")
		fonotecaObjectId := fonotecaMatches[1]
		cacheRequestUri := fmt.Sprintf("https://www.fonoteca.ch/cgi-bin/oecgi4.exe/inet_mpncache?MPN_ID=%s", fonotecaObjectId)
		resp, err := http.Get(cacheRequestUri)
		if err != nil {
			log.Fatal(err)
		}
		fonotecaUrl, err := ioutil.ReadAll(resp.Body)
		resp.Body.Close()
		if err != nil {
			log.Fatal(err)
		} else {
			uri, err := url.Parse(string(fonotecaUrl))
			if err != nil {
				ms.log.Fatal("uri is no valid Fonoteca url!")
			} else {
				me.URI = *uri
			}

		}
	}

	if me.Type == "" {
		me.Type = "unknown"
	}
	if actions, ok := ms.actions[me.Type]; ok {
		for _, a := range actions {
			ok, err := a.Do(w, req, me, action, params...)
			if err != nil {
				ms.DoPanicf(w, http.StatusInternalServerError, "Cannot execute action %s on signature %s: %v", action, me.Signature, err)
				return
			}
			if ok {
				return
			}
		}
	}
	ms.DoPanicf(w, http.StatusInternalServerError, "no handler for action %s on signature %s (%v) found - allowed actions: %v", action, me.Signature, me.Type, ms.ListActions())
	return
}
