// This file is part of Memobase Mediaserver which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memostream

import (
	"errors"
	"fmt"
	"github.com/goph/emperror"
	"net/url"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

// instance of Filesystem for "normal" disk filesystems
type FilesystemDisk struct {
	// directory mapping structure
	mapping map[string]string
}

// constructor for FilesystemDisk
func NewFilesystemDisk(mapping map[string]string) *FilesystemDisk {
	return &FilesystemDisk{mapping: mapping}
}

// mapping from uri to path
func (fm *FilesystemDisk) mapUri(uri url.URL) (string, error) {
	if uri.Scheme != "file" {
		return "", errors.New(fmt.Sprintf("cannot handle scheme %s: need file scheme", uri.Scheme))
	}
	var filename string
	var ok bool
	if uri.Host != "" {
		filename, ok = fm.mapping[strings.ToLower(uri.Host)]
		if !ok {
			return "", errors.New(fmt.Sprintf("no fspool for %s", uri.Host))
		}
	}
	filename = filepath.Join(filename, uri.Path)
	filename = filepath.Clean(filename)
	if runtime.GOOS == "windows" {
		filename = strings.TrimPrefix(filename, string(os.PathSeparator))
	}
	return filename, nil
}

// builds native access path
func (fm *FilesystemDisk) Truename(uri url.URL) string {
	path, err := fm.mapUri(uri)
	if err != nil {
		return "invalid"
	}
	return filepath.ToSlash(filepath.Clean(path))
}

// checks existence of file
func (fm *FilesystemDisk) Exist(uri url.URL) bool {
	path, err := fm.mapUri(uri)
	if err != nil {
		return false
	}
	if _, err := os.Stat(path); err != nil {
		return false
	}
	return true
}

// creates ReadSeekerCloser, returns last modification time
func (fm *FilesystemDisk) Open(uri url.URL) (ReadSeekerCloser, time.Time, error) {
	path, err := fm.mapUri(uri)
	if err != nil {
		return nil, time.Now(), emperror.Wrapf(err, "cannot map filepath from url %s", uri.String())
	}
	f, err := os.Open(path)
	if err != nil {
		return nil, time.Now(), emperror.Wrapf(err, "cannot open file %s", path)
	}
	stat, err := f.Stat()
	if err != nil {
		return nil, time.Now(), emperror.Wrapf(err, "cannot stat %s", path)
	}
	return f, stat.ModTime(), nil
}
