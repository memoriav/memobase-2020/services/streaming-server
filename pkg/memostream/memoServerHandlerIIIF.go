// This file is part of Memobase Mediaserver which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memostream

import (
	"bytes"
	"github.com/goph/emperror"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"path/filepath"
	"strings"
)

func (ms *memoServer) proxyIIIF(req *http.Request, writer http.ResponseWriter, signature, file, params string, public bool) ([]byte, error) {
	// create path as url part valid for mediaserver
	//file := strings.Replace(_file, "$", "%24", -1)
	file = strings.TrimPrefix(file, ms.iiifBase)
	//filePath := file
	if len(params) > 0 { // append parameters
		file = SingleJoiningSlash(strings.Replace(file, "/", "$", -1), params)
	}
	// prepend base url
	urlstring := SingleJoiningSlash(ms.iiifUrl, file)

	client := &http.Client{}
	ms.log.Infof("Proxy: %v", urlstring)
	req2, err := http.NewRequest("GET", urlstring, nil)
	if err != nil {
		return nil, emperror.Wrapf(err, "Error creating new request")
		//ms.DoPanicf(writer, http.StatusInternalServerError, "Error creating new request: %v", err)
		//return
	}

	newtoken := "open"
	if !public {
		sub := signature // ms.iiifJwtSubPrefix + filePath
		// NewJWT(secret string, subject string, alg string, valid int64, domain string, issuer string)
		newtoken, err = NewJWT(ms.jwtSecret, sub, ms.jwtAlg[0], 7200, "", "")
		if err != nil {
			return nil, emperror.Wrapf(err, "Error creating access token")
		}
	}
	// build headers to send cantaloupe the real url's to use
	//proto, host, port := ms.getProtoHostPort(req)
	baseurl, err := url.Parse(ms.baseUrl)
	if err != nil {
		return nil, emperror.Wrapf(err, "Cannot parse baseurl: %s", ms.baseUrl)
	}
	req2.Header.Add("X-Forwarded-Proto", baseurl.Scheme)
	req2.Header.Add("X-Forwarded-Host", baseurl.Hostname())
	req2.Header.Add("X-Forwarded-Port", baseurl.Port())
	req2.Header.Add("X-Forwarded-Path", SingleJoiningSlash(baseurl.RawPath, SingleJoiningSlash(ms.iiifPrefix, signature+"/"+newtoken)+"/"))
	req2.Header.Add("X-Forwarded-For", req.RemoteAddr[:strings.IndexByte(req.RemoteAddr, ':')])
	rs, err := client.Do(req2)
	if err != nil {
		return nil, emperror.Wrapf(err, "Error calling proxy: %s", urlstring)
	}

	defer rs.Body.Close()

	buf, err := ioutil.ReadAll(rs.Body)
	rs.Body = ioutil.NopCloser(bytes.NewBuffer(buf))
	io.Copy(writer, rs.Body)
	if err != nil {
		ms.log.Error(err)
	}
	return buf, nil
}

// IIIF handler
/*
Tile-Source: https://mediaserver.memobase.ch/sig-01/iiif/info.json?token=eyJhbG[...]6yJV_adQssw5c
Tile-URL: https://mediaserver.memobase.ch/redirIIIF/sig-01/eyJhbG[...]6yJV_adQssw5c/iiif/2/diplomhgk%24data%24e%24e%24ee572414d3bc2bc3d7bb1b28c22afddc.tiff/512,1024,512,512/512,/0/default.jpg
*/
func (ms *memoServer) HandlerIIIF(writer http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	file, ok := vars["file"]
	if !ok {
		ms.DoPanicf(writer, http.StatusBadRequest, "no file in request: %ms", req.URL.RawPath)
		return
	}
	signature, ok := vars["signature"]
	if !ok {
		ms.DoPanicf(writer, http.StatusBadRequest, "no signature in request: %ms", req.URL.RawPath)
		return
	}
	token, ok := vars["token"]
	if !ok {
		ms.DoPanicf(writer, http.StatusBadRequest, "no token in request: %ms", req.URL.RawPath)
		return
	}
	params, ok := vars["params"]
	if !ok {
		ms.DoPanicf(writer, http.StatusBadRequest, "no params in request: %ms", req.URL.RawPath)
		return
	}
	writer.Header().Set("Server", VERSION)
	writer.Header().Set("Access-Control-Allow-Origin", "*")

	//	writer.Header().Set("Access-Control-Allow-Origin", "*")

	me, err := ms.resolver.resolve(signature)
	if err != nil {
		ms.DoPanicf(writer, http.StatusNotFound, "signature %v not found: %v", signature, err)
		return
	}
	if me.Access != Media_Public {
		if err := CheckJWT(token, ms.jwtSecret, ms.jwtAlg, signature, "sub"); err != nil {
			ms.DoPanicf(writer, http.StatusForbidden, "Access denied: token check failed: %v", err)
			return
		}
	}

	var sigfile string
	var filename string
	if me.Protocol == Media_File {
		if !ms.fspool.Exist(me.URI) {
			ms.DoPanicf(writer, http.StatusNotFound, "file not found: %v", me.URI)
			return
		}
		sigfile = ms.fspool.Truename(me.URI) // me.getFilePath()
		filename = filepath.ToSlash(filepath.Clean(filepath.Join(ms.iiifBase, strings.Replace(file, "$", "/", -1))))
	} else {
		sigfile = me.URI.String()
		filename = strings.Replace(file, "$", "/", -1)
	}

	// create a correct filepath with slashes
	// TODO: Find better solution than commenting out
	// sigfile, err = url.QueryUnescape(sigfile)
	if err != nil {
		ms.DoPanicf(writer, http.StatusForbidden, "cannot unescape url %s", sigfile)
		return
	}
	if sigfile != filename {
		ms.DoPanicf(writer, http.StatusForbidden, "Don't cheat!! signature %s does not match path %s", sigfile, filename)
		return
	}

	if _, err := ms.proxyIIIF(req, writer, signature, strings.Replace(file, "$", "%24", -1), params, me.Access == Media_Public); err != nil {
		ms.DoPanicf(writer, http.StatusInternalServerError, "cannot proxy request: %v", err)
		return
	}
}
