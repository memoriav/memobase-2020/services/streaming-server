// This file is part of Memobase Mediaserver which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memostream

import (
	"errors"
	"fmt"
	"github.com/goph/emperror"
	"net/url"
)

// protocol and access types need to be enums
type MediaProtocol int
type MediaAccess int
type MediaStatus int

const (
	Media_File        MediaProtocol = 0
	Media_Redirect    MediaProtocol = 1
	Media_Proxy       MediaProtocol = 2
	Media_ProxyDirect MediaProtocol = 3

	Media_Public  MediaAccess = 0
	Media_Private MediaAccess = 1

	Media_OK    MediaStatus = 0
	Media_Error MediaStatus = 1
	Media_New               = 2
)

var (
	// conversion string to protocol
	MediaProtocolString = map[string]MediaProtocol{
		"file":        Media_File,
		"redirect":    Media_Redirect,
		"proxy":       Media_Proxy,
		"proxydirect": Media_ProxyDirect,
	}
	// conversion protocol number to string
	MediaProtocolNum = map[MediaProtocol]string{
		Media_File:        "file",
		Media_Redirect:    "redirect",
		Media_Proxy:       "proxy",
		Media_ProxyDirect: "proxydirect",
	}

	// conversion string to access type
	MediaAccessString = map[string]MediaAccess{
		"public": Media_Public,
		"closed": Media_Private,
	}
	// conversion access number to access string
	MediaAccessNum = map[MediaAccess]string{
		Media_Public:  "public",
		Media_Private: "closed",
	}

	MediaStatusString = map[string]MediaStatus{
		"ok":    Media_OK,
		"error": Media_Error,
		"new":   Media_New,
	}
	MediaStatusNum = map[MediaStatus]string{
		Media_OK:    "ok",
		Media_Error: "error",
		Media_New:   "new",
	}
)

// Represents the data needed to stream media object
type MediaEntry struct {
	Signature               string
	URI                     url.URL
	Protocol                MediaProtocol
	Access                  MediaAccess
	Status                  MediaStatus
	Type                    string
	Mimetype                string
	Width, Height, Duration int64
	ManifestV2, ManifestV3  string
}

func NewMediaEntry(signature, uri, access, protocol, status, t, mimetype string, width, height, length int64, manifestV2, manifestV3 string) (*MediaEntry, error) {
	p, ok := MediaProtocolString[protocol]
	// invalid data in database
	if !ok {
		return nil, errors.New(fmt.Sprintf("invalid protocol value %s", protocol))
	}

	a, ok := MediaAccessString[access]
	// invalid data in database
	if !ok {
		return nil, errors.New(fmt.Sprintf("invalid access value %s", access))
	}

	s, ok := MediaStatusString[status]
	// invalid data in database
	if !ok {
		return nil, errors.New(fmt.Sprintf("invalid statuss value %s", status))
	}

	// Create url and check uri syntax
	url, err := url.Parse(uri)
	if err != nil {
		return nil, emperror.Wrapf(err, "cannot parse uri %s", uri)
	}
	if url == nil {
		return nil, emperror.Wrapf(err, "url from uri is nil %s", uri)
	}
	return &MediaEntry{
		Signature:  signature,
		URI:        *url,
		Protocol:   p,
		Access:     a,
		Status:     s,
		Type:       t,
		Mimetype:   mimetype,
		Width:      width,
		Height:     height,
		Duration:   length,
		ManifestV2: manifestV2,
		ManifestV3: manifestV3,
	}, nil
}

func (me *MediaEntry) getProtocolString() string {
	return MediaProtocolNum[me.Protocol]
}

func (me *MediaEntry) getAccessString() string {
	return MediaAccessNum[me.Access]
}
