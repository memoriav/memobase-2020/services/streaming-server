// This file is part of Memobase Mediaserver which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memostream

import (
	"errors"
	"fmt"
	"github.com/bluele/gcache"
	"github.com/goph/emperror"
	"time"
)

// the ResolvarCache contains the database resolver
// and a caching structure
type ResolverCache struct {
	r     ResolverDB
	cache gcache.Cache
}

// create a new ResolverCache
// todo: expiration konfigurierbar
func NewResolver(r ResolverDB, cacheSize int, cachetimeout time.Duration) *ResolverCache {
	resolver := &ResolverCache{
		r:     r,
		cache: gcache.New(cacheSize).ARC().Expiration(cachetimeout).Build(),
	}
	return resolver
}

// resolves MediaEntry for signature and uses Cache if possible
func (r *ResolverCache) resolve(signature string) (*MediaEntry, error) {
	// look into cache
	me, err := r.cache.Get(signature)
	// cache fail
	if err != nil {
		// load from database
		me2, err := r.r.Resolve(signature)
		if err != nil {
			return nil, emperror.Wrapf(err, "cannot Resolve %s", signature)
		}
		// add to cache
		if err := r.cache.Set(signature, me2); err != nil {
			return nil, emperror.Wrapf(err, "cannot cache %s", signature)
		}
		return me2, nil
	}
	// typecast cache entry
	me3, ok := me.(*MediaEntry)
	// should never happen
	if !ok {
		return nil, errors.New(fmt.Sprintf("invalid type of MediaEntry: %v", me))
	}

	return me3, nil
}

// if data changes, we have to clear the cache to prevent the use of old content
func (r *ResolverCache) ClearCache() {
	r.cache.Purge()
}

func (r *ResolverCache) TestData(mediatype, access string, num int64) ([]*MediaEntry, error) {
	return r.r.TestData(mediatype, access, num)
}
