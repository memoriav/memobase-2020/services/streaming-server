package memostream

import (
	"context"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/op/go-logging"
	"io"
	"net"
	"net/http"
	"regexp"
	"text/template"
	"time"
)

type tokenServiceServer struct {
	srv           *http.Server
	host          string
	port          string
	jwtSecret     string
	jwtAlg        string
	jwtValid      time.Duration
	jwtIssuer     string
	log           *logging.Logger
	accesslog     io.Writer
	errorTemplate *template.Template
}

func NewTokenServiceServer(
	addr string,
	jwtSecret string,
	jwtAlg string,
	jwtValid time.Duration,
	jwtIssuer string,
	log *logging.Logger,
	accesslog io.Writer,
	errorTemplate string,
) *tokenServiceServer {
	host, port, err := net.SplitHostPort(addr)
	if err != nil {
		log.Panicf("cannot split address %s: %v", addr, err)
		return nil
	}
	return &tokenServiceServer{
		//mh:            NewMemoHandler(baseDir, urlPrefix, resolver, jwtSecret, log, errorTemplate),
		host:          host,
		port:          port,
		jwtSecret:     jwtSecret,
		jwtAlg:        jwtAlg,
		jwtValid:      jwtValid,
		jwtIssuer:     jwtIssuer,
		log:           log,
		accesslog:     accesslog,
		errorTemplate: template.Must(template.ParseFiles(errorTemplate)),
	}
}

func (tss *tokenServiceServer) DoPanicf(writer http.ResponseWriter, status int, message string, a ...interface{}) (err error) {
	msg := fmt.Sprintf(message, a...)
	tss.DoPanic(writer, status, msg)
	return
}

func (tss *tokenServiceServer) DoPanic(writer http.ResponseWriter, status int, message string) (err error) {
	type errData struct {
		Status     int
		StatusText string
		Message    string
	}
	tss.log.Error(message)
	data := errData{
		Status:     status,
		StatusText: http.StatusText(status),
		Message:    message,
	}
	writer.WriteHeader(status)
	// if there'tss no error Template, there'tss no help...
	tss.errorTemplate.Execute(writer, data)
	return
}

func (tss *tokenServiceServer) ListenAndServe(cert, key string) error {
	router := mux.NewRouter()

	//	router.PathPrefix(tss.urlPrefix).HandlerFunc(tss.mainHandler)
	//	router.HandleFunc("/", tss.commandHandler)

	// route for IIIF
	// https://identifier.memobase.ch/[domain]/[signature]
	tokenRegexp := regexp.MustCompile("^/([^/]+)/(.+)$")
	router.
		Methods("GET").
		MatcherFunc(func(r *http.Request, rm *mux.RouteMatch) bool {
			matches := tokenRegexp.FindSubmatch([]byte(r.URL.Path))
			if len(matches) == 0 {
				return false
			}
			rm.Vars = map[string]string{}
			rm.Vars["domain"] = string(matches[1])
			rm.Vars["sub"] = string(matches[2])
			return true
		}).
		HandlerFunc(tss.HandlerToken)

	// metric beat handler
	router.HandleFunc("/debug/vars", metricsHandler)

	loggedRouter := handlers.LoggingHandler(tss.accesslog, router)
	addr := net.JoinHostPort(tss.host, tss.port)
	tss.srv = &http.Server{
		Handler: loggedRouter,
		Addr:    addr,
	}
	if cert != "" && key != "" {
		tss.log.Infof("starting HTTPS memoServer at https://%v", addr)
		return tss.srv.ListenAndServeTLS(cert, key)
	} else {
		tss.log.Infof("starting HTTP memoServer at http://%v", addr)
		return tss.srv.ListenAndServe()
	}
}

func (tss *tokenServiceServer) HandlerToken(writer http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	domain, ok := vars["domain"]
	if !ok {
		tss.DoPanicf(writer, http.StatusBadRequest, "no domain in request: %ms", req.URL.RawPath)
		return
	}
	sub, ok := vars["sub"]
	if !ok {
		tss.DoPanicf(writer, http.StatusBadRequest, "no subject in request: %ms", req.URL.RawPath)
		return
	}

	token, err := NewJWT(tss.jwtSecret, sub, tss.jwtAlg, int64(tss.jwtValid.Seconds()), domain, tss.jwtIssuer)
	if err != nil {
		tss.DoPanicf(writer, http.StatusInternalServerError, "cannot create token: %v", err)
		return
	}

	writer.Write([]byte(token))
}

func (tss *tokenServiceServer) Shutdown(ctx context.Context) error {
	return tss.srv.Shutdown(ctx)
}
