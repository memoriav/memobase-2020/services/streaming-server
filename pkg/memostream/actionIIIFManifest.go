package memostream

import (
	"fmt"
	"net/http"
	"regexp"
)

// image Action using IIIF server for image manipulation
type ActionIIIFManifest struct {
	ms *memoServer
}

func NewActionIIIFManifest(ms *memoServer) *ActionIIIFManifest {
	am := &ActionIIIFManifest{ms: ms}
	return am
}

func (am *ActionIIIFManifest) GetType() []string {
	return []string{"image", "video", "audio", "unknown"}
}

var regexpManifestReplace = regexp.MustCompile(`{{\.(IIIFRendering|IIIFManifest|IIIFImage)}}`)

func (am *ActionIIIFManifest) Do(w http.ResponseWriter, req *http.Request, me *MediaEntry, action string, params ...string) (bool, error) {
	if action != "manifest" {
		return false, nil
	}

	if len(params) == 0 {
		params = append(params, "v2")
	}
	var manifest string
	switch params[0] {
	case "v2":
		manifest = me.ManifestV2
	case "v3":
		manifest = me.ManifestV3
	default:
		return false, fmt.Errorf("invalid manifest version (v2/v3 allowed): %v", params[1])
	}

	result := ReplaceAllStringSubmatchFunc(regexpManifestReplace, manifest, func(groups []string) string {
		if len(groups) < 2 {
			return "invalid replacement"
		}
		switch groups[1] {
		case "IIIFRendering":
			return am.ms.iiifRenderingBase
		case "IIIFManifest":
			return am.ms.iiifManifestBase
		case "IIIFImage":
			return fmt.Sprintf("%s%s", am.ms.baseUrl, am.ms.urlPrefix)
		}

		return fmt.Sprintf("cannot replace %v", groups[0])
	})

	w.Header().Set("Content-type", "application/json")
	w.Write([]byte(result))

	return true, nil
}
