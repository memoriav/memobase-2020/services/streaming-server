package memostream

import (
	"bytes"
	"fmt"
	"github.com/bluele/gcache"
	"github.com/op/go-logging"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

var verbose = false

var passthruRequestHeaderKeys = [...]string{
	"Accept",
	"Accept-Encoding",
	"Accept-Language",
	"Cache-Control",
	// "Cookie", // memo-proxy does not like cookies
	"Referer",
	"User-Agent",
	"Range",
	"Sec-Fetch-Dest",
	"Sec-Fetch-Mode",
	"Sec-Fetch-Site",
}

var passthruResponseHeaderKeys = [...]string{
	"Accept-Ranges",
	"Content-Encoding",
	"Content-Language",
	"Content-Type",
	"Content-Length",
	"Content-Range",
	// "Cache-Control",
	"Date",
	"Etag",
	"Expires",
	"Last-Modified",
	// "Location", // redirect not allowed in memo-proxy
	"Server",
	"Vary",
}

var requestProxyCache = gcache.New(100).LRU().Expiration(time.Minute * 10).Build()

func requestProxy(url *url.URL, w http.ResponseWriter, r *http.Request, insecure bool, log *logging.Logger) {
	urlstr := url.String()
	log.Debugf("requestProxy %s --> %s - %v", r.URL.String(), urlstr, r.Header)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Errorf("cannot read request body: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// Construct filtered header to send to origin server
	hh := http.Header{}
	for _, hk := range passthruRequestHeaderKeys {
		if hv, ok := r.Header[hk]; ok {
			hh[hk] = hv
		}
	}

	if loc, err := requestProxyCache.Get(urlstr); err == nil {
		if nurl, err := url.Parse(loc.(string)); err == nil {
			log.Debugf("url redirection cache hit %s --> %s", urlstr, nurl.String())
			url = nurl
		} else {
			log.Errorf("cannot parse cache url %s --> %s", urlstr, loc.(string))
		}
	}

	// Construct request to send to origin server
	rr := http.Request{
		Method: r.Method,
		URL:    url,
		Header: hh,
		Body:   ioutil.NopCloser(bytes.NewReader(body)),
		// TODO: Is this correct for a 0 value?
		//       Perhaps a 0 may need to be reinterpreted as -1?
		ContentLength: int64(len(body)),
		Close:         r.Close,
	}

	// create a transport clone and set insecure flag
	transport, ok := http.DefaultTransport.(*http.Transport)
	if !ok {
		log.Error("http.DefaultTransport is not (*http.Transport)")
		http.Error(w, "http.DefaultTransport is not (*http.Transport)", 500)
		return
	}
	transport = transport.Clone()
	transport.TLSClientConfig.InsecureSkipVerify = insecure

	// Forward request to origin server
	resp, err := transport.RoundTrip(&rr)
	if err != nil {
		// TODO: Passthru more error information
		log.Errorf("Could not reach origin server %v: %s", url.String(), err.Error())
		http.Error(w, "Could not reach origin server", 500)
		return
	}
	location := resp.Header.Get("Location")
	numRedirects := 0
	for location != "" {
		numRedirects++
		log.Debugf("Redirect #%v: %s", numRedirects, location)
		if numRedirects > 5 {
			log.Errorf("too many redirects")
			http.Error(w, "too many redirects", 500)
			return
		}
		resp.Body.Close()
		url, err = url.Parse(location)
		if numRedirects > 5 {
			log.Errorf("cannot parse redirect %s", location)
			http.Error(w, "cannot parse redirect", 500)
			return
		}
		rr.Body.Close()
		rr := http.Request{
			Method: "GET",
			URL:    url,
			Header: hh,
			Body:   ioutil.NopCloser(bytes.NewReader(body)),
			// TODO: Is this correct for a 0 value?
			//       Perhaps a 0 may need to be reinterpreted as -1?
			ContentLength: int64(len(body)),
			Close:         r.Close,
		}
		resp, err = http.DefaultTransport.RoundTrip(&rr)
		if err != nil {
			// TODO: Passthru more error information
			log.Errorf("could not reach origin server %s", url.String())
			http.Error(w, "Could not reach origin server", 500)
			return
		}
		location = resp.Header.Get("Location")
		if location == "" {
			if err := requestProxyCache.Set(urlstr, url.String()); err != nil {
				log.Errorf("cannot cache url %s --> %s", urlstr, location)
			}
		}
	}
	defer resp.Body.Close()

	log.Debugf("<-- %v %+v\n", resp.Status, resp.Header)

	// Transfer filtered header from origin server -> client
	respH := w.Header()
	for _, hk := range passthruResponseHeaderKeys {
		if hv, ok := resp.Header[hk]; ok {
			respH[hk] = hv
		}
	}
	w.WriteHeader(resp.StatusCode)

	// Transfer response from origin server -> client
	if resp.ContentLength > 0 {
		// (Ignore I/O errors, since there's nothing we can do)
		if _, err := io.CopyN(w, resp.Body, resp.ContentLength); err != nil {
			log.Debugf("cannot copy content: %s", err.Error())
			http.Error(w, fmt.Sprintf("cannot copy content: %s", err.Error()), http.StatusInternalServerError)
			return
		}
	} else if resp.Close { // TODO: Is this condition right?
		// Copy until EOF or some other error occurs
		for {
			if _, err := io.Copy(w, resp.Body); err != nil {
				log.Debugf("cannot copy last content: %s", err.Error())
				break
			}
		}
	} else {
		// (Ignore I/O errors, since there's nothing we can do)
		if _, err := io.Copy(w, resp.Body); err != nil {
			log.Debugf("cannot copy content: %s", err.Error())
			http.Error(w, fmt.Sprintf("cannot copy content: %s", err.Error()), http.StatusInternalServerError)
			return
		}
	}
}
