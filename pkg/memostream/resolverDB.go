// This file is part of Memobase Mediaserver which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memostream

// for a type to be a ResolverDB there must be a function for resolving
// a signature and a close function for shtutdown
type ResolverDB interface {
	Resolve(signature string) (*MediaEntry, error)
	TestData(mediatype, access string, num int64) ([]*MediaEntry, error)
	Close()
}
