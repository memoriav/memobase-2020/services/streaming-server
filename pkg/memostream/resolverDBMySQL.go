// This file is part of Memobase Mediaserver which is released under GPLv3.
// See file license.txt for full license details.
//
// Author Juergen Enge <juergen@info-age.net>
//
// This code uses elements from
// * "Mediaserver" (Center for Digital Matter HGK FHNW, Basel)
// * "Remote Exhibition Project" (info-age GmbH, Basel)
//

package memostream

import (
	"database/sql"
	"fmt"
	"github.com/goph/emperror"
)

// resolver for MySQL database (should work with PostGreSQL too)
type ResolverDBMySQL struct {
	// sql query to retrieve uri, access and protocol (in this order) with ? parameter for signature
	query        string
	db           *sql.DB
	preparedStmt *sql.Stmt
}

// create new ResolverDBMySQL
func NewResolverDBMysql(db *sql.DB, query string) (*ResolverDBMySQL, error) {
	rdm := &ResolverDBMySQL{
		db:    db,
		query: query,
	}
	return rdm, rdm.Init()
}

// initializes ResolverDBMySQL
func (rdm *ResolverDBMySQL) Init() (err error) {
	// prepare Statement for resolver
	rdm.preparedStmt, err = rdm.db.Prepare(rdm.query)
	if err != nil {
		return emperror.Wrapf(err, "cannot prepare stmt %s", rdm.query)
	}
	return nil
}

// shutdown resolver
func (rdm *ResolverDBMySQL) Close() {
	// nothing to do
}

// resolve MediaEntry for signature
func (rdm *ResolverDBMySQL) Resolve(signature string) (*MediaEntry, error) {
	// use prepared statement for speed
	row := rdm.preparedStmt.QueryRow(signature)
	//row := rdm.db.QueryRow(rdm.query, signature)
	var access, protocol, uri, status string
	var typ, mimetype sql.NullString
	var width, height, duration sql.NullInt64
	var manifestV2, manifestV3 sql.NullString
	// mapUri data
	switch err := row.Scan(&uri, &access, &protocol, &status, &typ, &mimetype, &width, &height, &duration, &manifestV2, &manifestV3); err {
	case sql.ErrNoRows: // dataset not found
		return nil, emperror.Wrapf(err, "cannot find signature %s", signature)
	case nil: // all fine, dataset found
		if !typ.Valid && protocol == "file" {
			return nil, fmt.Errorf("no metadata for signature %s", signature)
		}
		return NewMediaEntry(
			signature,
			uri,
			access,
			protocol,
			status,
			typ.String,
			mimetype.String,
			width.Int64,
			height.Int64,
			duration.Int64,
			manifestV2.String,
			manifestV3.String)
	default: // something strange happenz
		return nil, emperror.Wrapf(err, "error querying %s - %s", rdm.query, signature)
	}
}

func (rdm *ResolverDBMySQL) TestData(mediatype, access string, num int64) ([]*MediaEntry, error) {
	sqlstr := "SELECT sig, `uri`, `access`, `proto` AS protocol, `status`, `type`, `mimetype`, `width`, `height`, `duration`, `manifest_v2`, `manifest_v3`" +
		" FROM entities_metadata" +
		" WHERE 1=1"
	params := []interface{}{}
	if mediatype != "" {
		params = append(params, mediatype)
		sqlstr += " AND `type`=?"
	}
	if access != "" {
		params = append(params, access)
		sqlstr += " AND access=?"
	}

	sqlstr += " ORDER BY RAND()"

	params = append(params, num)
	sqlstr += " LIMIT 0, ?"

	rows, err := rdm.db.Query(sqlstr, params...)
	if err != nil {
		return nil, emperror.Wrapf(err, "cannot execute %v - %v", sqlstr, params)
	}
	defer rows.Close()

	entries := []*MediaEntry{}
	for rows.Next() {
		var sig, access, protocol, uri, status string
		var typ, mimetype sql.NullString
		var width, height, duration sql.NullInt64
		var manifestV2, manifestV3 sql.NullString
		// mapUri data
		if err := rows.Scan(&sig, &uri, &access, &protocol, &status, &typ, &mimetype, &width, &height, &duration, &manifestV2, &manifestV2); err != nil {
			return nil, emperror.Wrapf(err, "cannot scan row")
		}
		m, err := NewMediaEntry(
			sig,
			uri,
			access,
			protocol,
			status,
			typ.String,
			mimetype.String,
			width.Int64,
			height.Int64,
			duration.Int64,
			manifestV2.String,
			manifestV3.String)
		if err != nil {
			return nil, emperror.Wrapf(err, "cannot create new mediaentry")
		}
		entries = append(entries, m)
	}
	return entries, nil
}
