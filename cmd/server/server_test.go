package main

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/phayes/freeport"
	"gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server/pkg/memostream"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"testing"
)

var db *sql.DB
var prep *sqlmock.ExpectedPrepare
var addr string

func TestMain( m *testing.M) {
	workingDirectory, err := os.Getwd()
	if err != nil {
		log.Fatalf("error getting working directory: %v", err)
	}

	db, mock, err := sqlmock.New()
	if err != nil {
		log.Fatalf("an error '%v' was not expected when opening a stub database connection", err)
	}
	defer db.Close()



	prep = mock.ExpectPrepare("SELECT uri, access, proto AS protocol, `status` FROM test.entities WHERE sig = ?")
	resolverDB, err := memostream.NewResolverDBMysql(db, "SELECT uri, access, proto AS protocol, `status` FROM test.entities WHERE sig = ?")
	if err != nil {
		log.Fatalf("cannot initialize resolverdb: %v", err)
	}
	defer resolverDB.Close()


	log, lf := memostream.CreateLogger("memostream", "", "DEBUG")
	defer lf.Close()


	resolver := memostream.NewResolver(resolverDB, 10)
	fm := memostream.NewFileMapper(map[string]string{"x": fmt.Sprintf("%s/../../web/static", workingDirectory)})

	port, err := freeport.GetFreePort()
	if err != nil {
		log.Fatalf("cannot find free port: %v", err)
	}
	addr = fmt.Sprintf("localhost:%v", port)

	srv, err := memostream.NewServer(
		"/memo/",
		"/command/",
		"/iiif/",
		"",
		"",
		"iiif:",
		fmt.Sprintf("%s/../../web/template/openseadragon.gohtml", workingDirectory),
		addr,
		resolver,
		fm,
		"swordfish",
		[]string{"HS386"},
		log,
		os.Stdout,
		fmt.Sprintf("%s/../../web/template/error.gohtml", workingDirectory),
		fmt.Sprintf("%s/../../web/template/videojs.gohtml", workingDirectory),
		fmt.Sprintf("%s/../../web/template/audiohowler.gohtml", workingDirectory),
		"/static/",
		fmt.Sprintf("%s/../../web/static", workingDirectory),
	)
	if err != nil {
		log.Fatalf("error initializing server: %v", err)
	}

	var shutdown = false
	go func() {
		if err := srv.ListenAndServe("", ""); err != nil {
			if !shutdown {
				log.Errorf("server died: %+v", err)
			}
		}
		fmt.Println("server stopped")
	}()

	exitCode := m.Run()

	shutdown = true
	srv.Shutdown(context.Background())
	os.Exit(exitCode)
}

func TestProxy(t *testing.T) {
	url := fmt.Sprintf("http://%s/memo/sig-001", addr)
	t.Logf("query %s - proxy", url)
	prep.
		ExpectQuery().
		WithArgs("sig-001").
		WillReturnRows(sqlmock.NewRows([]string{"uri", "access", "protocol", "status"}).
			AddRow(fmt.Sprintf("http://%s/static/memoriav_logo_400x400.jpg", addr), "public", "proxy", "ok"))
	resp, err := http.Get(url)
	if err != nil {
		t.Errorf("error checking proxy: %v", err)
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Errorf("error checking proxy - invalid status: %v %v", resp.StatusCode, resp.Status)
		return
	}
	num, err := io.Copy(ioutil.Discard, resp.Body)
	if err != nil {
		t.Errorf("error checking proxy - cannot read data: %v", err)
		return
	}
	if num != 19337 {
		t.Errorf("error checking proxy - invalid size %v", num)
		return
	}
}

func TestFile(t *testing.T) {
	url := fmt.Sprintf("http://%s/memo/sig-002", addr)
	t.Logf("query %s - file", url)
	prep.
		ExpectQuery().
		WithArgs("sig-002").
		WillReturnRows(sqlmock.NewRows([]string{"uri", "access", "protocol", "status"}).
			AddRow("file://x/memoriav_logo_400x400.jpg", "public", "file", "ok"))
	resp, err := http.Get(url)
	if err != nil {
		t.Errorf("error checking file: %v", err)
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Errorf("error checking file - invalid status: %v %v", resp.StatusCode, resp.Status)
		return
	}
	num, err := io.Copy(ioutil.Discard, resp.Body)
	if err != nil {
		t.Errorf("error checking file - cannot read data: %v", err)
		return
	}
	if num != 19337 {
		t.Errorf("error checking file - invalid size %v", num)
		return
	}
}

func TestRedirect(t *testing.T) {
	url := fmt.Sprintf("http://%s/memo/sig-003", addr)
	t.Logf("query %s - redirect", url)
	prep.
		ExpectQuery().
		WithArgs("sig-003").
		WillReturnRows(sqlmock.NewRows([]string{"uri", "access", "protocol", "status"}).
			AddRow(fmt.Sprintf("http://%s/static/memoriav_logo_400x400.jpg", addr), "public", "redirect", "ok"))
	resp, err := http.Get(url)
	if err != nil {
		t.Errorf("error checking redirect: %v", err)
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		t.Errorf("error checking redirect - invalid status: %v %v", resp.StatusCode, resp.Status)
	}
	num, err := io.Copy(ioutil.Discard, resp.Body)
	if err != nil {
		t.Errorf("error checking redirect - cannot read data: %v", err)
		return
	}
	if num != 19337 {
		t.Errorf("error checking redirect - invalid size %v", num)
		return
	}
}


