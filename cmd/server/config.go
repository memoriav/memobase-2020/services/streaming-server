package main

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server/pkg/memostream"
	"log"
	"os"
	"strings"
	"time"
)

type duration struct {
	time.Duration
}

func (d *duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))
	return err
}

type CfgResolverDBMySQL struct {
	Dsn            string
	ConnMaxTimeout duration
	Schema         string
	Query          string
	Tls            bool
}

type IIIFConfig struct {
	Prefix         string
	Base           string
	Cached         string
	Url            string
	JwtSubPrefix   string
	ViewerTemplate string
	ManifestBase   string `toml:"manifest"`
	RenderingBase  string `toml:"rendering"`
}

type FileMap struct {
	Alias  string
	Folder string
}

type Endpoint struct {
	Host string `toml:"host"`
	Port int    `toml:"port"`
}

type SSHTunnel struct {
	User           string   `toml:"user"`
	PrivateKey     string   `toml:"privatekey"`
	LocalEndpoint  Endpoint `toml:"localendpoint"`
	ServerEndpoint Endpoint `toml:"serverendpoint"`
	RemoteEndpoint Endpoint `toml:"remoteendpoint"`
}

type Config struct {
	Logfile             string
	Loglevel            string
	AccessLog           string
	CertPEM             string
	KeyPEM              string
	Addr                string
	BaseUrl             string
	StaticDir           string
	UrlPrefix           string
	CmdPrefix           string
	StaticPrefix        string
	InsecureProxy       bool
	IIIF                IIIFConfig
	JwtKey              string
	JwtAlg              []string
	Signatures          map[string]memostream.Sig
	ResolverDBMySQL     CfgResolverDBMySQL
	ResolverCacheSize   int
	ErrorTemplate       string
	VideoViewerTemplate string
	AudioViewerTemplate string
	FileMap             []FileMap
	Viewer              map[string]string
	SSHTunnel           SSHTunnel `toml:"sshtunnel"`
	CacheTimeout        duration  `toml:"cachetimeout"`
}

func LoadConfig(filepath string) Config {
	var conf Config
	conf.IIIF.Prefix = "/iiif/"
	conf.CmdPrefix = "/cmd/"
	conf.UrlPrefix = "/memo/"
	conf.CacheTimeout.Duration = 3 * time.Minute
	conf.InsecureProxy = false

	_, err := toml.DecodeFile(filepath, &conf)
	if err != nil {
		log.Fatalln("Error on loading config: ", err)
	}
	jwtKey, exists := os.LookupEnv("JWT_KEY")
	if exists {
		log.Printf("Load JWT key from envvar JWT_KEY")
		conf.JwtKey = jwtKey
	} else {
		log.Printf("Load JWT key from config file")
	}
	jwtAlg, exists := os.LookupEnv("JWT_ALGORITHM")
	if exists {
		log.Printf("Load JWT algorithm from envvar JWT_ALGORITHM")
		conf.JwtAlg = strings.Split(jwtAlg, ",")
	} else {
		log.Printf("Load JWT algorithm from config file")
	}
	dsn, exists := os.LookupEnv("DSN")
	if exists {
		log.Printf("Load DSN from envvar DSN")
		conf.ResolverDBMySQL.Dsn = dsn
		// removed because of security issues with password in logfile
		//log.Printf("DSN from Environment: [%s]", conf.ResolverDBMySQL.Dsn)
	} else {
		mariadbUser, exists := os.LookupEnv("MARIADB_USER")
		if !exists {
			log.Fatalln("No Mariadb user defined!")
		} else {
			log.Printf("Load MariaDB user from envvar MARIADB_USER")
		}
		mariadbPassword, exists := os.LookupEnv("MARIADB_PASSWORD")
		if !exists {
			log.Fatalln("No Mariadb password defined!")
		} else {
			log.Printf("Load MariaDB password from envvar MARIADB_PASSWORD")
		}
		mariadbHost, exists := os.LookupEnv("MARIADB_HOST")
		if !exists {
			log.Fatalln("No Mariadb host defined!")
		} else {
			log.Printf("Load MariaDB host from envvar MARIADB_HOST")
		}
		mariadbPort, exists := os.LookupEnv("MARIADB_PORT")
		if !exists {
			log.Fatalln("No Mariadb port defined!")
		} else {
			log.Printf("Load MariaDB port from envvar MARIADB_PORT")
		}
		mariadbDatabase, exists := os.LookupEnv("MARIADB_DATABASE")
		if !exists {
			log.Fatalln("No Mariadb database defined!")
		} else {
			log.Printf("Load MariaDB database from envvar MARIADB_DATABASE")
		}
		if conf.ResolverDBMySQL.Tls {
			conf.ResolverDBMySQL.Dsn = fmt.Sprintf(
				"%s:%s@tcp(%s:%s)/%s?tls=custom",
				strings.Trim(mariadbUser, "\n"),
				strings.Trim(mariadbPassword, "\n"),
				mariadbHost,
				mariadbPort,
				mariadbDatabase,
			)
		} else {
			conf.ResolverDBMySQL.Dsn = fmt.Sprintf(
				"%s:%s@tcp(%s:%s)/%s",
				strings.Trim(mariadbUser, "\n"),
				strings.Trim(mariadbPassword, "\n"),
				mariadbHost,
				mariadbPort,
				mariadbDatabase,
			)
		}
		// removed because of security issues with password in logfile
		log.Printf("DSN from Config: [%s]", conf.ResolverDBMySQL.Dsn)
	}

	return conf
}
