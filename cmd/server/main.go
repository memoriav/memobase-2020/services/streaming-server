package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"database/sql"
	"flag"
	"io"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
	"gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server/pkg/memostream"
	sshtunnel "gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server/pkg/sshTunnel"
)

func main() {
	configFile := flag.String("cfg", "./memostream.toml", "config file location")
	flag.Parse()

	var exPath = ""
	// if configfile not found try path of executable as prefix
	if !memostream.FileExists(*configFile) {
		ex, err := os.Executable()
		if err != nil {
			panic(err)
		}
		exPath = filepath.Dir(ex)
		if memostream.FileExists(filepath.Join(exPath, *configFile)) {
			*configFile = filepath.Join(exPath, *configFile)
		} else {
			log.Fatalf("cannot find configuration file: %v", *configFile)
			return
		}
	}
	// configfile should exists at this place
	var config Config
	config = LoadConfig(*configFile)

	// create logger instance
	log, lf := memostream.CreateLogger("memostream", config.Logfile, config.Loglevel)
	defer lf.Close()

	if config.SSHTunnel.User != "" && config.SSHTunnel.PrivateKey != "" {
		tunnels := map[string]*sshtunnel.SourceDestination{}
		tunnels["postgres"] = &sshtunnel.SourceDestination{
			Local: &sshtunnel.Endpoint{
				Host: config.SSHTunnel.LocalEndpoint.Host,
				Port: config.SSHTunnel.LocalEndpoint.Port,
			},
			Remote: &sshtunnel.Endpoint{
				Host: config.SSHTunnel.RemoteEndpoint.Host,
				Port: config.SSHTunnel.RemoteEndpoint.Port,
			},
		}
		tunnel, err := sshtunnel.NewSSHTunnel(
			config.SSHTunnel.User,
			config.SSHTunnel.PrivateKey,
			&sshtunnel.Endpoint{
				Host: config.SSHTunnel.ServerEndpoint.Host,
				Port: config.SSHTunnel.ServerEndpoint.Port,
			},
			tunnels,
			log,
		)
		if err != nil {
			log.Errorf("cannot create sshtunnel %v@%v:%v - %v", config.SSHTunnel.User, config.SSHTunnel.ServerEndpoint.Host, &config.SSHTunnel.ServerEndpoint.Port, err)
			return
		}
		if err := tunnel.Start(); err != nil {
			log.Errorf("cannot create sshtunnel %v - %v", tunnel.String(), err)
			return
		}
		defer tunnel.Close()
		time.Sleep(2 * time.Second)
	}

	var resolverDB memostream.ResolverDB
	log.Infof("DNS Resolver: %v", config.ResolverDBMySQL.Dsn)
	if config.ResolverDBMySQL.Dsn == "" {
		resolverDB = memostream.NewResolverDBStatic(config.Signatures)
	} else {
		// declare parameter ...?tls=custom in dsn to use certificate
		if strings.Contains(config.ResolverDBMySQL.Dsn, "tls=custom") {
			rootCertPool := x509.NewCertPool()
			pem, err := os.ReadFile("/etc/ssl/certs/ca-certificates.crt")
			if err != nil {
				log.Panicf("error reading ca certificates: %v", err)
				return
			}
			if ok := rootCertPool.AppendCertsFromPEM(pem); !ok {
				log.Panicf("can't append certificates to store")
				return
			}
			mysql.RegisterTLSConfig("custom", &tls.Config{RootCAs: rootCertPool})
		}
		db, err := sql.Open("mysql", config.ResolverDBMySQL.Dsn)
		if err != nil {
			// don't write dsn in error message due to password inside
			log.Panicf("error connecting to database: %v", err)
			return
		}
		defer db.Close()
		if err := db.Ping(); err != nil {
			log.Panicf("cannot ping database: %v", err)
			return
		}
		db.SetConnMaxLifetime(time.Duration(config.ResolverDBMySQL.ConnMaxTimeout.Duration))

		resolverDB, err = memostream.NewResolverDBMysql(
			db,
			config.ResolverDBMySQL.Query,
		)
		if err != nil {
			// don't write dsn in error message due to password inside
			log.Panicf("cannot initialize ResolverDBMySQL: %v", err)
			return
		}
	}
	defer resolverDB.Close()
	resolver := memostream.NewResolver(resolverDB, config.ResolverCacheSize, config.CacheTimeout.Duration)

	var accesslog io.Writer
	if config.AccessLog == "" {
		accesslog = os.Stdout
	} else {
		f, err := os.OpenFile(config.AccessLog, os.O_WRONLY|os.O_CREATE, 0755)
		if err != nil {
			log.Panicf("cannot open file %s: %v", config.AccessLog, err)
			return
		}
		defer f.Close()
		accesslog = f
	}

	fspool := memostream.NewFilesystemPool()

	mapping := map[string]string{}
	for _, val := range config.FileMap {
		mapping[strings.ToLower(val.Alias)] = val.Folder
	}
	fsd := memostream.NewFilesystemDisk(mapping)

	fspool.SetFilesystem("file", fsd)

	srv, err := memostream.NewServer(
		config.BaseUrl,
		config.UrlPrefix,
		config.CmdPrefix,
		config.IIIF.Prefix,
		config.IIIF.Base,
		config.IIIF.Cached,
		config.IIIF.Url,
		config.IIIF.JwtSubPrefix,
		config.IIIF.ViewerTemplate,
		config.IIIF.ManifestBase,
		config.IIIF.RenderingBase,
		config.Addr,
		config.InsecureProxy,
		resolver,
		fspool,
		config.JwtKey,
		config.JwtAlg,
		log,
		accesslog,
		config.ErrorTemplate,
		config.Viewer,
		config.StaticPrefix,
		config.StaticDir,
	)
	if err != nil {
		log.Errorf("error initializing server: %v", err)
		return
	}

	// add some helpful actions
	srv.AddAction(memostream.NewActionImageIIIF(srv))
	srv.AddAction(memostream.NewActionVideo(srv))
	srv.AddAction(memostream.NewActionAudio(srv))
	srv.AddAction(memostream.NewActionExternal(srv))
	srv.AddAction(memostream.NewActionMaster(srv))
	srv.AddAction(memostream.NewActionIIIFManifest(srv))

	log.Infof("registered actions: %v", srv.ListActions())

	go func() {
		if err := srv.ListenAndServe(config.CertPEM, config.KeyPEM); err != nil {
			log.Errorf("server died: %v", err)
		}
	}()

	end := make(chan bool, 1)

	// process waiting for interrupt signal (TERM or KILL)
	go func() {
		sigint := make(chan os.Signal, 1)

		// interrupt signal sent from terminal
		signal.Notify(sigint, os.Interrupt)

		signal.Notify(sigint, syscall.SIGTERM)
		signal.Notify(sigint, syscall.SIGKILL)

		<-sigint

		// We received an interrupt signal, shut down.
		log.Infof("shutdown requested")
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()

		srv.Shutdown(ctx)

		end <- true
	}()

	<-end
	log.Info("server stopped")
}
