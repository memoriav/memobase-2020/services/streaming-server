module gitlab.switch.ch/memoriav/memobase-2020/services/streaming-server

go 1.23.2

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/bluele/gcache v0.0.0-20190518031135-bc40bd653833
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/goph/emperror v0.17.2
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/phayes/freeport v0.0.0-20180830031419-95f893ade6f2
	golang.org/x/crypto v0.0.0-20180904163835-0709b304e793
)

require (
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/pkg/errors v0.8.0 // indirect
)
