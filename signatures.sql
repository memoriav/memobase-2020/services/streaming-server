-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host:
-- Erstellungszeit: 12. Mrz 2020 um 18:21
-- Server-Version: 10.3.17-MariaDB-1:10.3.17+maria~bionic-log
-- PHP-Version: 7.2.23-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `test`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `test2`
--

CREATE TABLE `test2` (
  `sig` varchar(255) COLLATE utf8_bin NOT NULL,
  `uri` varchar(1024) COLLATE utf8_bin NOT NULL,
  `access` enum('closed','public') COLLATE utf8_bin NOT NULL,
  `proto` enum('file','redirect','proxy') COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Daten für Tabelle `test2`
--

INSERT INTO `test2` (`sig`, `uri`, `access`, `proto`) VALUES
('sig-01', 'file://C:/daten/Persoenlich/bilder/MobilePhotos/16-09-03 13-41-58 0634.mp4', 'public', 'file'),
('sig-02', 'https://ba14ns21403.fhnw.ch/video/open/performance/2002_B_B_Yours_Sincerly.mov.mp4', 'public', 'redirect'),
('sig-03', 'file://C:/daten/Persoenlich/bilder/MobilePhotos/16-09-03 13-41-58 0634.mp4', 'closed', 'file'),
('sig-04', 'https://ba14ns21403.fhnw.ch/video/open/performance/2002_B_B_Yours_Sincerly.mov.mp4', 'public', 'proxy');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `test2`
--
ALTER TABLE `test2`
  ADD PRIMARY KEY (`sig`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
