-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host:
-- Erstellungszeit: 23. Apr 2020 um 16:24
-- Server-Version: 10.3.17-MariaDB-1:10.3.17+maria~bionic-log
-- PHP-Version: 7.2.23-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `test`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `sig` varchar(255) COLLATE utf8_bin NOT NULL,
  `error` tinyint(1) NOT NULL DEFAULT 0,
  `mimetype` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `uri` varchar(1024) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `entities`
--

CREATE TABLE IF NOT EXISTS `entities` (
  `sig` varchar(255) COLLATE utf8_bin NOT NULL,
  `uri` varchar(1024) COLLATE utf8_bin NOT NULL,
  `manifest` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `access` enum('closed','public') COLLATE utf8_bin NOT NULL,
  `proto` enum('file','redirect','proxy') COLLATE utf8_bin NOT NULL,
  `status` enum('ok','error','new') COLLATE utf8_bin NOT NULL DEFAULT 'new',
  `errormessage` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `lastcheck` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastchange` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `creationtime` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Stellvertreter-Struktur des Views `entities_metadata`
-- (Siehe unten für die tatsächliche Ansicht)
--
CREATE TABLE IF NOT EXISTS `entities_metadata` (
`sig` varchar(255)
,`uri` varchar(1024)
,`access` enum('closed','public')
,`proto` enum('file','redirect','proxy')
,`status` enum('ok','error','new')
,`error` tinyint(1)
,`mimetype` varchar(128)
,`width` int(11)
,`height` int(11)
,`metadata` longtext
,`modificationtime` timestamp
);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `metadata`
--

CREATE TABLE IF NOT EXISTS `metadata` (
  `sig` varchar(255) COLLATE utf8_bin NOT NULL,
  `error` tinyint(1) NOT NULL DEFAULT 0,
  `mimetype` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `metadata` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `modificationtime` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktur des Views `entities_metadata`
--
DROP TABLE IF EXISTS `entities_metadata`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`ba14ns21404.fhnw.ch` SQL SECURITY DEFINER VIEW IF NOT EXISTS `entities_metadata`  AS  select `e`.`sig` AS `sig`,`e`.`uri` AS `uri`,`e`.`access` AS `access`,`e`.`proto` AS `proto`,`e`.`status` AS `status`,`m`.`error` AS `error`,`m`.`mimetype` AS `mimetype`,`m`.`width` AS `width`,`m`.`height` AS `height`,`m`.`metadata` AS `metadata`,`m`.`modificationtime` AS `modificationtime` from (`entities` `e` left join `metadata` `m` on(`m`.`sig` = `e`.`sig`)) ;

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`sig`),
  ADD KEY `error` (`error`);

--
-- Indizes für die Tabelle `entities`
--
ALTER TABLE `entities`
  ADD PRIMARY KEY (`sig`),
  ADD KEY `status` (`status`),
  ADD KEY `lastcheck` (`lastcheck`),
  ADD KEY `lastchange` (`lastchange`),
  ADD KEY `creationtime` (`creationtime`);

--
-- Indizes für die Tabelle `metadata`
--
ALTER TABLE `metadata`
  ADD PRIMARY KEY (`sig`),
  ADD KEY `error` (`error`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
